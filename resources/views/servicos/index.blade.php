@extends('layouts.app')

@section('content')


<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-4 col-md-4 col-xl-4 text-left">
                            <h3 style="margin-bottom:10px"> Serviços </h3>
                        </div>
                        <div class="col-sm-8 col-md-8 col-xl-8 text-right">
                            <a class="btn btn-warning" id="pesquisar" name="pesquisar" href="javascript:jQuery('#filter').toggle('1000')" role="button">Pesquisar</a>
                            <a class="btn btn-primary" id="novo" name="novo" href="{{route('servicos.create')}}" role="button">Novo</a>
                        </div>
                    </div> 

                    <div id="filter" class="card" style="margin-top:20px; display:none">
                        <div class="card-body">
                            @component('components.form', ['action'=>route('servicos.index'), 'method'=>'GET'])
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    @include('components.inputs', ['name' => 'nome', 'type' => 'text', 'inputClass'=>'form-control-sm', 'value'=>request()->get('nome') ])
                                </div>
                                <div class='col-xs-12 col-md-12 text-right' style="margin-top: 10px;">
                                    @include('components.inputs', ['type'=>'submit', 'value'=>'Filtrar', 'name'=>'filtrar', 'inputClass'=>'btn-sm'] )
                                </div>
                            @endcomponent
                            </div>
                        </div>
                    </div>

                    @if(Session::has('message'))
                    @include('components.alerts', ['data'=>Session::get('message')])
                    @endif
                    @if($servicos->count() > 0)
                    <div class="table-responsive" style="margin-top:20px">
                        <table class="table table-hover table-striped table-sm">
                            <thead>
                                <tr class="table-primary">
                                    <th scope="col">ID</th>
                                    <th scope="col">Nome</th>
                                    <th scope="col">Tempo estimado</th>
                                    <th scope="col">Preço</th>
                                    <th scope="col">Opções</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach ($servicos as $servico)
                                <tr>
                                    <th scope="row">{{$servico->id}}</th>
                                    <td>{{$servico->nome}}</td>
                                    <td>{{$servico->tempo_estimado}} {{$servico->unidadeTempo->sigla}}</td>
                                    <td>R$ {{$servico->preco}}</td>
                                    <td> 
                                        @include('components.buttons', ['type'=>'edit', 'href'=>route('servicos.edit', $servico->id)]) </a>
                                        @include('components.buttons', ['type'=>'delete', 'href'=>route('servicos.destroy', $servico->id), 'id'=>$servico->id, 'message'=>'Deseja excluir o serviço "'. $servico->produtoServico->nome.'"?']) 
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                        <div class='col-xs-12 col-md-12 text-center'>
                            {{ $servicos->links() }}
                        </div>

                        <footer class="blockquote-footer"> Total: {{$servicos->total() > 1 ? $servicos->total() . ' registros encontrados.' : '1 registro encontrado.' }} </footer>
                    </div>
                    @else
                    <div class="alert alert-warning" role="alert" style="margin-top:20px">
                        Nenhum registro encontrado
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection