@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-4 col-md-4 col-xl-4 text-left">
                            <h3 style="margin-bottom:10px"> Serviço </h3> Edição
                        </div>
                    </div>

                    @component('components.form', ['action'=>route('servicos.update', $servico->id), 'method'=>'PUT'])
                    <div class="form-group row offset-1">
                        <div class="col-sm-5">
                            @include('components.inputs', ['name' => 'nome', 'type' => 'text', 'value'=>$servico->produtoServico->nome])
                        </div>
                        <div class="col-sm-5">
                            @include('components.inputs', ['name' => 'preco', 'type' => 'text', 'label' => 'Preço', 'value'=>$servico->preco])
                        </div>
                        <div class="col-sm-5">
                            @include('components.inputs', ['name' => 'tempo_estimado', 'type' => 'text', 'label' => 'Tempo estimado', 'value'=>$servico->tempo_estimado])
                        </div>
                        <div class='col-sm-5'>
                            @include('components.inputs', ['name' => 'unidade_tempo', 'type' => 'select', 'label' => 'Unidade tempo', 'value'=>$servico->id_unidade_tempo, 'optionKey'=>'id', 'optionValue'=>'nome', 'options'=>$unidades_tempo])
                        </div>
                        <div class="col-sm-5">
                            @include('components.inputs', ['name' => 'pontos_ganhos', 'type' => 'text', 'label' => 'Pontos ganhos', 'value'=>$servico->pontos_ganhos])
                        </div>
                        <div class="col-sm-5">
                            @include('components.inputs', ['name' => 'pontos_necessarios', 'type' => 'text', 'label' => 'Pontos necessários', 'value'=>$servico->pontos_necessarios])
                        </div>
                        <div class='col-sm-5'>
                            @include('components.inputs', ['name' => 'atendido_por', 'type' => 'select', 'label' => 'Atendido por', 'value'=>$servico->id_perfil, 'optionKey'=>'id', 'optionValue'=>'nome', 'options'=>$perfis])
                        </div>
                        <div class='col-sm-5'>
                            @include('components.inputs', ['name' => 'ativo', 'type' => 'checkbox', 'value'=>$servico->produtoServico->ativo])
                        </div>
                        <div class='col-sm-10 text-right' style="margin-top: 10px;">
                            @include('components.buttons', ['type'=>'cancel', 'value'=>'Cancelar'] )
                            @include('components.inputs', ['type'=>'submit', 'id'=>'submit', 'value'=>'Salvar'] )
                        </div>
                    </div>
                    @endcomponent
                </div>
            </div>
        </div>
    </div>
</div>


@endsection