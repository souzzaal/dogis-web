@extends('layouts.app')

@section('content')


<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-4 col-md-4 col-xl-4 text-left">
                            <h3 style="margin-bottom:10px"> Agendamentos </h3>
                        </div>
                        <div class="col-sm-8 col-md-8 col-xl-8 text-right">                        
                            <a class="btn btn-warning" id="pesquisar" name="pesquisar" href="javascript:jQuery('#filter').toggle('1000')" role="button">Pesquisar</a>
                            <a class="btn btn-primary" id="novo" name="novo" href="{{route('agendamentos.create')}}" role="button">Novo</a>
                        </div>
                    </div>

                    <div id="filter" class="card" style="margin-top:20px;">
                        <div class="card-body">
                            @component('components.form', ['action'=>route('agendamentos.index'), 'method'=>'GET'])
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    @include('components.inputs', ['name' => 'data', 'type' => 'date', 'inputClass'=>'form-control-sm', 'value'=>request()->get('data') ? request()->get('data') : date('Y-m-d') ])
                                </div>

                                <div class='col-xs-12 col-md-12 text-right' style="margin-top: 10px;">
                                    @include('components.inputs', ['type'=>'submit', 'value'=>'Filtrar', 'name'=>'filtrar' ,'inputClass'=>'btn-sm'] )
                                </div>
                            @endcomponent
                            </div>
                        </div>
                    </div>

                    @if(Session::has('message'))
                    @include('components.alerts', ['data'=>Session::get('message')])
                    @endif
                    @if($agendamentos->count() > 0)
                    <div class="table-responsive" style="margin-top:20px">
                        <table class="table table-hover table-striped table-sm">
                            <thead>
                                <tr class="table-primary">
                                    <th scope="col">ID</th>
                                    <th scope="col">Pet</th>
                                    <th scope="col">Tipo</th>
                                    <th scope="col">Cliente</th>
                                    <th scope="col">Serviços</th>
                                    <th scope="col">Opções</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach ($agendamentos as $agendamento)
                                <tr>
                                    <th scope="row">{{$agendamento->id}}</th>
                                    <td>{{$agendamento->pet->nome}}</td>
                                    <td>{{$agendamento->pet->tipo->nome}}</td>
                                    <td>{{$agendamento->pet->dono->nome}}</td>
                                    <td> 
                                        @foreach ($agendamento->servicos as $servico) 
                                        {{$servico->produtoServico->nome}} &bull; {{$servico->horario}} &bull; {{$servico->veterinario->nome}}<br/>
                                        @endforeach
                                    </td>
                                    <td> 
                                        @include('components.buttons', ['type'=>'edit', 'href'=>route('agendamentos.edit', $agendamento->id)]) </a>
                                        @include('components.buttons', ['type'=>'delete', 'href'=>route('agendamentos.destroy', $agendamento->id), 'id'=>$agendamento->id, 'message'=>'Deseja excluir o agendamento "'. $agendamento->id.'"?']) 
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                        <div class='col-xs-12 col-md-12 text-center'>
                            {{ $agendamentos->links() }}
                        </div>

                        <footer class="blockquote-footer"> Total: {{$agendamentos->total() > 1 ? $agendamentos->total() . ' registros encontrados.' : '1 registro encontrado.' }} </footer>
                    </div>
                    @else
                    <div class="alert alert-warning" role="alert" style="margin-top:20px">
                        Nenhum registro encontrado
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection