@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-4 col-md-4 col-xl-4 text-left">
                            <h3 style="margin-bottom:10px"> Agendamento </h3>
                        </div>
                    </div>

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    <span id="grupo-servico" class="form-group row col-sm-12" style="display:none">
                        <div class='col-sm-3'>
                            @include('components.inputs', ['id'=>'servico', 'name'=>'servico[]', 'label'=>'Serviço', 'type'=>'select', 'value'=>null, 'optionKey'=>'id', 'optionValue'=>'nome', 'options'=>$servicos])
                        </div>
                        <div class='col-sm-3'>
                            @include('components.inputs', ['id'=>'funcionario', 'name'=>'funcionario[]', 'label'=>'Funcionário', 'type'=>'select', 'value'=>null, 'optionKey'=>'id', 'optionValue'=>'nome', 'options'=>$funcionarios])
                        </div>
                        <div class="col-sm-2">
                            @include('components.inputs', ['id'=>'horario', 'name'=>'horario[]', 'label'=>'Horário', 'type'=>'time'])
                        </div>
                        <div class='col-sm-3'>
                            @include('components.inputs', ['id'=>'situacao', 'name'=>'situacao[]', 'label'=>'Situação', 'type'=>'select', 'value'=>null, 'optionKey'=>'id', 'optionValue'=>'nome', 'options'=>$situacoes])
                        </div>
                    </span>
                    @component('components.form', ['action'=>route('agendamentos.store'), 'method'=>'POST'])
                    <div class="form-group row col-sm-12 ">
                        <div class="col-sm-5">
                            @include('components.inputs', ['name'=>'data', 'type'=>'date', 'value'=>date('Y-m-d')])
                        </div>
                        <div class="col-sm-5">
                            @include('components.inputs', ['name'=>'pet', 'type'=>'select', 'label'=>'Pet', 'value'=>null, 'optionKey'=>'id', 'optionValue'=>'nome', 'options'=>$pets])
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <a id="incluir" name="incluir" class="btn btn-primary btn-sm form-control" href="#" onclick="incluir()"> <i class="fas fa-plus"> </i> </a>
                            </div>
                        </div>

                        <div id='servicos' class="form-group row col-sm-12">

                        </div>

                        <div class='col-sm-12 text-right' style="margin-top: 10px;">
                            @include('components.buttons', ['type'=>'cancel', 'value'=>'Cancelar'] )
                            @include('components.inputs', ['type'=>'submit', 'id'=>'submit', 'value'=>'Salvar'] )
                        </div>

                    </div>
                    <script type="text/javascript">
                        window.onload = () => {
                            incluir()
                        }

                        incluir = () => {

                            let a = document.createElement('a')
                            a.classList.add('btn', 'btn-danger', 'btn-sm', 'form-control')
                            a.onclick = (e) => {
                                if (confirm('Deseja remover o serviço?')) {
                                    e.path[0].closest("#grupo-servico").remove()
                                }
                            }
                            a.href = '#'
                            a.innerHTML = '<i class="fa fa-trash" aria-hidden="true"> </i>';

                            let divfg = document.createElement('div')
                            divfg.classList.add('form-group')
                            divfg.appendChild(a)

                            let div = document.createElement('div')
                            div.classList.add('col-sm-1')
                            div.appendChild(divfg)

                            let grupoServico = document.querySelector('#grupo-servico').cloneNode(true)
                            grupoServico.style.display = '';
                            grupoServico.appendChild(div)

                            document.querySelector('#servicos')
                                .appendChild(grupoServico);
                        }
                    </script>
                    @endcomponent
                </div>
            </div>
        </div>
    </div>
</div>


@endsection