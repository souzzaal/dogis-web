@if($type == 'text' || $type == 'date' || $type =='time')
<div class="form-group">
    <label for="{{$name}}" class="col-form-label {{ $labelClass ?? 'col-form-label-sm'}}">{{ $label ?? ucfirst($name) }}</label>
    <input type="{{$type}}" id="{{$id ?? $name ?? ''}}" name="{{$name ?? ''}}" class="form-control @error($name) is-invalid @enderror {{$inputClass ?? ''}}" value="{{old($name) ?? $value ?? ''}}">

    @error($name)
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>
@endif

@if($type == 'checkbox')
<div class="form-check">
    @if($value || old($name))
    <input type="checkbox" id="{{$id ?? $name ?? ''}}" name="{{$name ?? ''}}" class="form-check-input @error($name) is-invalid @enderror {{$inputClass ?? ''}}"  checked>
    @else
    <input type="checkbox" id="{{$id ?? $name ?? ''}}" name="{{$name ?? ''}}" class="form-check-input @error($name) is-invalid @enderror {{$inputClass ?? ''}}">
    @endif
    <label for="{{$name}}" class="col-form-label {{ $labelClass ?? 'col-form-label-sm'}}">{{ $label ?? ucfirst($name) }}</label>

    @error($name)
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>
@endif

@if($type == 'select') 
<div class="form-group">
<label for="{{$name}}" class="col-form-label {{ $labelClass ?? 'col-form-label-sm'}}">{{ $label ?? ucfirst($name) }}</label>
<select id="{{$id ?? $name ?? ''}}" name="{{$name ?? ''}}" class="form-control @error($name) is-invalid @enderror select-one" data-value="" style="width: 100%;">
    @foreach($options as $option)
        @if($option->$optionKey == $value)
        <option value="{{$option->$optionKey}}" selected>{{$option->$optionValue}}</option>
        @else
        <option value="{{$option->$optionKey}}">{{$option->$optionValue}}</option>
        @endif
    @endforeach
</select>

@error($name)
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
@enderror
</div>
@endif

@if($type == 'submit')
    <input type="submit" id="{{$id ?? $name ?? ''}}" name="{{$name ?? ''}}" class="btn btn-primary {{ $inputClass ?? '' }}" value="{{$value}}"/>
@endif

@if($type == 'textArea')
<div class="form-group">
    <label class="col-form-label {{ $labelClass ?? 'col-form-label-sm'}}" for="{{$name}}">{{ $label ?? ucfirst($name) }}</label>
    <textarea id="{{$id ?? $name ?? ''}}" name="{{$name ?? ''}}" class="form-control @error($name) is-invalid @enderror" rows="3">{{ $value ?? ''}}</textarea>
    @error($name)
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
</div>
@endif

