
<form action="{{$action}}" method="{{strtoupper(trim($method)) == 'GET' ? 'GET' : 'POST'}}">

    @csrf

    @if(strtoupper(trim($method))=='PUT')
        @method('PUT')
    @endif

    @if(strtoupper(trim($method))=='DELETE')
        @method('DELETE')
    @endif
    
    {{ $slot }}

</form>