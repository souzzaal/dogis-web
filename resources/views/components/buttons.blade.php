@if($type == 'edit')
<a class="btn btn-primary btn-sm" id="{{$id ?? $name ?? ''}}" name="{{$name ?? ''}}" href="{{$href}}"> <i class="fas fa-pencil-alt"> </i> </a>
@endif

@if($type == 'cancel')
    <input type="button" id="{{$id ?? $name ?? ''}}" name="{{$name ?? ''}}" class="btn {{ $inputClass ?? '' }}" value="{{$value ?? 'Cancelar'}}" onclick="javascript:history.go(-1)"/>
@endif

@if($type == 'delete')
<a class="btn btn-danger btn-sm" id="{{$id ?? $name ?? ''}}" name="{{$name ?? ''}}"  href="#" onclick="javascript:exclui('{{$href}}', '{{$id}}', '{{$message}}')"> <i class="fas fa-trash"> </i> </a>
<script type="text/javascript">
   function exclui(url, id, message) {
        if (confirm(message)) {
            $.ajax({
                type: "DELETE",
                url: url,
                data: {
                    '_token': "{{ csrf_token() }}",
                    'id': id
                },
            }).then((e)=>{
                location.reload();
                // console.log(e);
            }).catch((err)=>{
                // console.log('erro', err)
            });
        }
   }
</script>
@endif