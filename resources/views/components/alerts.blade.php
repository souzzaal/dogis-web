

<div style="margin-top:10px" class="alert alert-{{$data['type'] ?? ''}}" role="alert">
  {{$data['message'] ?? ''}}
</div>