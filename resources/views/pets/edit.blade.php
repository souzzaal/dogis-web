@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-4 col-md-4 col-xl-4 text-left">
                            <h3 style="margin-bottom:10px"> Pet </h3>
                        </div>
                    </div>

                    @component('components.form', ['action'=>route('pets.update', $pet->id), 'method'=>'PUT'])
                    <div class="form-group row offset-1">
                        <div class="col-sm-5">
                            @include('components.inputs', ['name'=>'nome', 'type'=>'text', 'value'=>$pet->nome])
                        </div>
                        <div class="col-sm-5">
                        @include('components.inputs', ['name'=>'tipo_pet', 'type'=>'select', 'label'=>'Tipo pet', 'value'=>$pet->id_tipo_pet, 'optionKey'=>'id', 'optionValue'=>'nome', 'options'=>$tipos])
                        </div>
                        <div class="col-sm-5">
                            @include('components.inputs', ['name'=>'porte', 'type'=>'text', 'value'=>$pet->porte])
                        </div>
                        <div class='col-sm-5'>
                            @include('components.inputs', ['name'=>'cliente', 'type'=>'select', 'value'=>$pet->id_pessoa, 'optionKey'=>'id', 'optionValue'=>'nome', 'options'=>$clientes])
                        </div>
                        <div class="col-sm-10">
                            @include('components.inputs', ['name'=>'observacoes', 'label'=>'Observações', 'type'=>'textArea', 'value'=>$pet->observacoes])
                        </div>
                        <div class='col-sm-5'>
                            @include('components.inputs', ['name'=>'permite_foto', 'label'=>'Permite foto', 'type'=>'checkbox', 'value'=>$pet->permite_foto])
                        </div>
                        <div class='col-sm-5'>
                            @include('components.inputs', ['name'=>'ativo', 'type'=>'checkbox', 'value'=>$pet->ativo])
                        </div>
                        <div class='col-sm-10 text-right' style="margin-top: 10px;">
                            @include('components.buttons', ['type'=>'cancel', 'value'=>'Cancelar'] )
                            @include('components.inputs', ['type'=>'submit', 'id'=>'submit', 'value'=>'Salvar'] )
                        </div>
                    </div>
                    @endcomponent
                </div>
            </div>
        </div>
    </div>
</div>


@endsection