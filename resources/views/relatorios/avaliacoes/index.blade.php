@extends('layouts.app')

@section('content')

<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-xl-12 text-left">
                            <h3 style="margin-bottom:10px"> Relatório de avaliações por período </h3>
                        </div>
                    </div>

                    <div id="filter" class="card" style="margin-top:20px;">
                        <div class="card-body">
                            @component('components.form', ['action'=>route('relatorio_avaliacoes'), 'method'=>'GET'])
                            <div class="form-group row">
                            
                                <div class='col-sm-3'>
                                    @include('components.inputs', ['name'=>'profissional', 'label'=>'Profissional', 'type'=>'select', 'value'=>request()->get('profissional') ? request()->get('profissional') : null, 'optionKey'=>'id', 'optionValue'=>'nome', 'options'=>$funcionarios])
                                </div>
                                
                                <div class="col-sm-3">
                                    @include('components.inputs', ['name' => 'data_inicio', 'label'=>'Data início', 'type' => 'date', 'inputClass'=>'form-control', 'value'=>request()->get('data_inicio') ? request()->get('data_inicio') : date('Y-m-d') ])
                                </div>
                                
                                 <div class="col-sm-3">
                                    @include('components.inputs', ['name' => 'data_final', 'label'=>'Data final','type' => 'date', 'inputClass'=>'form-control', 'value'=>request()->get('data_final') ? request()->get('data_final') : date('Y-m-d') ])
                                </div>

                                <div class='col-xs-12 col-md-12 text-right' style="margin-top: 10px;">
                                    @include('components.inputs', ['type'=>'submit', 'value'=>'Filtrar', 'name'=>'filtrar' ,'inputClass'=>'btn-sm'] )
                                </div>
                            @endcomponent
                            </div>
                        </div>
                    </div>

                    @if(Session::has('message'))
                    @include('components.alerts', ['data'=>Session::get('message')])
                    @endif
                    @if($avaliacoes->count() > 0)
                    <div class="table-responsive" style="margin-top:20px">
                        <table class="table table-hover table-striped table-sm">
                            <thead>
                                <tr class="table-primary">
                                    <th scope="col">#</th>
                                    <th scope="col">Data</th>
                                    <th scope="col">Pet</th>
                                    <th scope="col">Tipo</th>
                                    <th scope="col">Cliente</th>
                                    <th scope="col">Serviço</th>
                                    <th scope="col">Pontuação</th>
                                </tr>
                            </thead>
                            <tbody>
                       
                                @foreach ($avaliacoes as $avaliacao)
                                <tr>
                                    <th scope="row">{{$loop->iteration}}</th>
                                    <td>{{$avaliacao->created_at->format('d/m/Y H:i')}}</td>
                                    <td>{{$avaliacao->agendamentoServico->agendamento->pet->nome}}</td>
                                    <td>{{$avaliacao->agendamentoServico->agendamento->pet->tipo->nome}}</td>
                                    <td>{{$avaliacao->agendamentoServico->agendamento->pet->dono->nome}}</td>
                                    <td>{{$avaliacao->agendamentoServico->produtoServico->nome}}</td>
                                    <td>{{$avaliacao->pontuacao}}</td>
                                </tr>
                                @endforeach
                                
                                <tr class="table-active">
                                    <th colspan=5></th>
                                    <td class="text-right">Média</td>
                                    <td>{{number_format($avaliacoes->avg('pontuacao'), 1,',','.')}}</td>
                                </tr>

                            </tbody>
                        </table>

                    </div>
                    @elseif($showAlert)
                    <div class="alert alert-warning" role="alert" style="margin-top:20px">
                        Nenhum registro encontrado
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection