<?php

namespace App\Http\Controllers;

use App\Http\Requests\ServicoRequest;
use App\Models\Servico;
use App\Repositories\ServicoRepository;
use Exception;
use Illuminate\Cache\RetrievesMultipleKeys;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Session;

class ServicoController extends Controller
{

    public function __construct()
    {
        $this->repository = new ServicoRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        try {
            $this->authorize('viewAny', Servico::class);
            return $this->repository->index($request);
        } catch (\Exception $e) {
            Session::flash('message', ['type' => 'danger', 'message' => "Permissão negada"]);
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $this->authorize('create', Servico::class);
            return $this->repository->create();
        } catch (\Exception $e) {
            Session::flash('message', ['type' => 'danger', 'message' => "Permissão negada"]);
            return redirect()->back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ServicoRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServicoRequest $request)
    {
        try {
            $this->authorize('create', Servico::class);
            $this->repository->store($request);
            return redirect()->route('servicos.index');
        } catch (\Exception $e) {
            Session::flash('message', ['type' => 'danger', 'message' => "Permissão negada"]);
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    { }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $this->authorize('view', [Servico::find($id), request()->user()]);
        } catch (\Exception $e) {
            Session::flash('message', ['type' => 'danger', 'message' => "Permissão negada"]);
            return redirect()->back();
        }

        try {
            $servico = Servico::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            Session::flash('message', ['type' => 'danger', 'message' => "Serviço não encontrado."]);
            return redirect()->route('servicos.index');
        }
        return $this->repository->edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ServicoRequest $request, $id)
    {
        try {
            $this->authorize('update', [Servico::find($id), $request->user()]);
        } catch (\Exception $e) {
            Session::flash('message', ['type' => 'danger', 'message' => "Permissão negada"]);
            return redirect()->back();
        }

        try {
            $servico = Servico::findOrFail($id);
        } catch (\Exception $e) {
            Session::flash('message', ['type' => 'danger', 'message' => "Serviço não encontrado."]);
            return redirect()->route('servicos.index');
        }

        $this->repository->update($request, $servico);
        return redirect()->route('servicos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->authorize('delete', [Servico::find($id), request()->user()]);
        } catch (\Exception $e) {
            Session::flash('message', ['type' => 'danger', 'message' => "Permissão negada"]);
        }

        try {
            $servico = Servico::findOrFail($id);
        } catch (\Exception $e) {
            Session::flash('message', ['type' => 'danger', 'message' => "Serviço não encontrado."]);
        }

        $servicoRequest = new ServicoRequest();
        $validator =  \Validator::make(request()->all(), $servicoRequest->rules(), $servicoRequest->messages());
        if ($validator->fails()) {
            Session::flash('message', ['type' => 'danger', 'message' => $validator->messages()->first()]);
        } else {
            $this->repository->destroy($servico);
        }

    }
}
