<?php

namespace App\Http\Controllers;

use App\Http\Requests\AgendamentoRequest;
use App\Models\Agendamento;
use App\Repositories\AgendamentoRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AgendamentoController extends Controller
{


    public function __construct()
    {
        $this->repository = new AgendamentoRepository();
    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $this->authorize('viewAny', Agendamento::class);
            return $this->repository->index($request);
        } catch (\Exception $e) {
            Session::flash('message', ['type' => 'danger', 'message' => "Permissão negada"]);
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $this->authorize('create', Agendamento::class);
            return $this->repository->create();
        } catch (\Exception $e) {
            dd($e);
            Session::flash('message', ['type' => 'danger', 'message' => "Permissão negada"]);
            return redirect()->back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AgendamentoRequest $request)
    {
        try {
            $this->authorize('create', Agendamento::class);
            $this->repository->store($request);
            return redirect()->route('agendamentos.index');
        } catch (\Exception $e) {
            dd($e);
            Session::flash('message', ['type' => 'danger', 'message' => "Permissão negada"]);
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        try {
            $this->authorize('view', [Agendamento::find($id), request()->user()]);
        } catch (\Exception $e) {
            Session::flash('message', ['type' => 'danger', 'message' => "Permissão negada"]);
            return redirect()->back();
        }

        try {
            $agendamento = Agendamento::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            Session::flash('message', ['type' => 'danger', 'message' => "Serviço não encontrado."]);
            return redirect()->back();
        }
        
        return $this->repository->edit($agendamento);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AgendamentoRequest $request, $id)
    {
        try {
            $this->authorize('update', [Agendamento::find($id), $request->user()]);
        } catch (\Exception $e) {
            Session::flash('message', ['type' => 'danger', 'message' => "Permissão negada"]);
            return redirect()->back();
        }

        try {
            $agendamento = Agendamento::findOrFail($id);
        } catch (\Exception $e) {
            Session::flash('message', ['type' => 'danger', 'message' => "Agendamento não encontrado."]);
            return redirect()->back()->withInput();
        }

        $this->repository->update($request, $agendamento);
        return redirect()->route('agendamentos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->authorize('delete', [Agendamento::find($id), request()->user()]);
        } catch (\Exception $e) {
            Session::flash('message', ['type' => 'danger', 'message' => "Permissão negada"]);
        }

        try {
            $agendamento = Agendamento::findOrFail($id);
        } catch (\Exception $e) {
            Session::flash('message', ['type' => 'danger', 'message' => "Agendamento não encontrado."]);
        }

        $agendamentoRequest = new AgendamentoRequest();
        $validator =  \Validator::make(['id'=>$id], $agendamentoRequest->rules(), $agendamentoRequest->messages());
        if ($validator->fails()) {
            Session::flash('message', ['type' => 'danger', 'message' => $validator->messages()->first()]);
        } else {
            $this->repository->destroy($agendamento);
        }
        
    }
}
