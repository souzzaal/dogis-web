<?php

namespace App\Http\Controllers;

use App\Http\Requests\PetRequest;
use App\Models\Pet;
use App\Repositories\PetRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PetController extends Controller
{


    public function __construct()
    {
        $this->repository = new PetRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $this->authorize('viewAny', Pet::class);
            return $this->repository->index($request);
        } catch (\Exception $e) {
            Session::flash('message', ['type' => 'danger', 'message' => "Permissão negada"]);
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $this->authorize('create', Pet::class);
            return $this->repository->create();
        } catch (\Exception $e) {
            Session::flash('message', ['type' => 'danger', 'message' => "Permissão negada"]);
            return redirect()->back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PetRequest $request)
    {
        try {
            $this->authorize('create', Pet::class);
            $this->repository->store($request);
            return redirect()->route('pets.index');
        } catch (\Exception $e) {
            Session::flash('message', ['type' => 'danger', 'message' => "Permissão negada"]);
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

        /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $this->authorize('view', [Pet::find($id), request()->user()]);
        } catch (\Exception $e) {
            Session::flash('message', ['type' => 'danger', 'message' => "Permissão negada"]);
            return redirect()->back();
        }

        try {
            $pet = Pet::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            Session::flash('message', ['type' => 'danger', 'message' => "Serviço não encontrado."]);
            return redirect()->back();
        }
        return $this->repository->edit($pet);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PetRequest $request, $id)
    {
        try {
            $this->authorize('update', [Pet::find($id), $request->user()]);
        } catch (\Exception $e) {
            Session::flash('message', ['type' => 'danger', 'message' => "Permissão negada"]);
            return redirect()->back();
        }

        try {
            $pet = Pet::findOrFail($id);
        } catch (\Exception $e) {
            Session::flash('message', ['type' => 'danger', 'message' => "Serviço não encontrado."]);
            return redirect()->back();
        }

        $this->repository->update($request, $pet);
        return redirect()->route('pets.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->authorize('delete', [Pet::find($id), request()->user()]);
        } catch (\Exception $e) {
            Session::flash('message', ['type' => 'danger', 'message' => "Permissão negada"]);
        }

        try {
            $pet = Pet::findOrFail($id);
        } catch (\Exception $e) {
            Session::flash('message', ['type' => 'danger', 'message' => "Pet não encontrado."]);
        }

        $petRequest = new PetRequest();
        $validator =  \Validator::make(request()->all(), $petRequest->rules(), $petRequest->messages());
        if ($validator->fails()) {
            Session::flash('message', ['type' => 'danger', 'message' => $validator->messages()->first()]);
        } else {
            $this->repository->destroy($pet);
        }

    }
}
