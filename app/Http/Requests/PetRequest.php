<?php

namespace App\Http\Requests;

use App\Rules\PetDeleteRule;
use App\Rules\PetPorteRule;
use Illuminate\Foundation\Http\FormRequest;

class PetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (request()->isMethod('post')) {
            
            return [
                'cliente' => 'required|numeric',
                'tipo_pet' => 'required|numeric',
                'nome' => 'required|string',
                'porte' => [new PetPorteRule]
            ];
        } elseif (request()->isMethod('put')) {
            
            return [
                'cliente' => 'required|numeric',
                'tipo_pet' => 'required|numeric',
                'nome' => 'required|string',
                'porte' => [new PetPorteRule]
            ];
        } else if (request()->isMethod('delete') ) {
            return [
                'id' => [new PetDeleteRule]
            ];
        }
    }
}
