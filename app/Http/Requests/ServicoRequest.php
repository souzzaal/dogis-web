<?php

namespace App\Http\Requests;

use App\Rules\ServicoDeleteRule;
use Illuminate\Foundation\Http\FormRequest;

class ServicoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (request()->isMethod('post') ) {
            return [
                'nome' => 'required|string|unique:produtos_servicos,nome',
                'tempo_estimado' => 'required|numeric',
                'pontos_ganhos' => 'required|numeric',
                'pontos_necessarios' => 'required|numeric',
                'preco' => ['required','regex:/^(((\d{1,3}\.)*(\d{3}))|(\d+)),\d{2}$/'],
                'atendido_por' => 'required|numeric',
                'unidade_tempo' => 'required|numeric',
                'ativo' => 'nullable', 

            ];
        } else if (request()->isMethod('put') ) {

            return [
                'nome' => 'required|string|unique:produtos_servicos,nome,'.request()->route()->servico,
                'tempo_estimado' => 'required|numeric',
                'pontos_ganhos' => 'required|numeric',
                'pontos_necessarios' => 'required|numeric',
                'preco' => ['required','regex:/^(((\d{1,3}\.)*(\d{3}))|(\d+)),\d{2}$/'],
                'atendido_por' => 'required|numeric',
                'unidade_tempo' => 'required|numeric',
                'ativo' => 'nullable',
            ];
        } else if (request()->isMethod('delete') ) {
            return [
                'id' => [new ServicoDeleteRule]
            ];
        }
    }
}
