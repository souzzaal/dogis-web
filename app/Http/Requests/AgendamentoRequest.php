<?php

namespace App\Http\Requests;

use App\Rules\AgendamentoServicoRule;
use App\Rules\AgendamentoDataUpdateRule;
use App\Rules\AgendamentoDeleteRule;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class AgendamentoRequest extends FormRequest
{
    
    private $hoje;
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->hoje = new Carbon();
        
        if (request()->isMethod('post')) {
            return [
                'data' => 'required|date_format:Y-m-d|after_or_equal:' . $this->hoje->format('Y-m-d'),
                'pet' => 'required|numeric',
                'servico' => ['required', new AgendamentoServicoRule],
                
            ];
        } elseif(request()->isMethod('put')) {
            return [
                'data' => ['required','date_format:Y-m-d', new AgendamentoDataUpdateRule],
                'pet' => 'required|numeric',
                'servico' => ['required', new AgendamentoServicoRule],
                
            ];
        } elseif(request()->isMethod('delete')) {
            return [
                'id' => [new AgendamentoDeleteRule]
            ];
        }
    }
    
    public function messages()
    {
        return [
            'data.after_or_equal' => 'Não é possível agendar para datas passadas'
        ];
    }
    
}
