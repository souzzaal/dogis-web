<?php

namespace App\Rules;

use App\Models\AgendamentoServico;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class AgendamentoDataUpdateRule implements Rule
{
    
    private $message;
    
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $agendamento = AgendamentoServico::where('id_agendamento', request()->route('agendamento'))
            ->first();
        $dataAgendamento = $agendamento->data_inicio->startOfDay();
        
        $hoje =  Carbon::now()->startOfDay();
        
        $dataNovoAgendamento = Carbon::parse($value)->startOfDay();
        
        if($dataAgendamento  >= $hoje && $dataNovoAgendamento < $hoje) {
             $this->message = 'Não é possível reagendar para uma data passada';
             return false;
        }
        
        if($dataAgendamento  < $hoje && $dataAgendamento != $dataNovoAgendamento) {
             $this->message = 'Não é possível alterar a data de um agendamento passado';
             return false;
        }
        
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
