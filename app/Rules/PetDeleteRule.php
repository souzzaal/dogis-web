<?php

namespace App\Rules;

use App\Models\Agendamento;
use App\Models\AgendamentoServico;
use Illuminate\Contracts\Validation\Rule;

class PetDeleteRule implements Rule
{

    private $message;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $agendamentos = Agendamento::whereIdPet(request()->route()->pet)->count();

        if ($agendamentos > 0) {
            $this->message = "O pet possui agendamentos";
            return false;
        }
        
        return true;
    }

    /** 
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
