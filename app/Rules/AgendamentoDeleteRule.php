<?php

namespace App\Rules;

use App\Models\AgendamentoServico;
use Illuminate\Contracts\Validation\Rule;

class AgendamentoDeleteRule implements Rule
{
    
    private $message;
    
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $agendamentosServicos  = AgendamentoServico::where('id_agendamento', request()->route('agendamento'))
            ->where('data_inicio', '<', date('Y-m-d H:i:s'))
            ->count();

        if($agendamentosServicos > 0) {
            $this->message = 'O agendamento possui um ou mais serviços com a data passada';
            return false;
        }
        
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
