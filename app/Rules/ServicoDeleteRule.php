<?php

namespace App\Rules;

use App\Models\AgendamentoServico;
use App\Models\Servico;
use Illuminate\Contracts\Validation\Rule;

class ServicoDeleteRule implements Rule
{

    private $message = null;


    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
    
        $agendamentosFuturo = AgendamentoServico::where('id_servico', request()->route()->servico)->count();

        if ($agendamentosFuturo) {
            $this->message = "O serviço possui agendamentos.";
            return false;
        }
        
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}