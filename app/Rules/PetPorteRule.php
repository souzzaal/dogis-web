<?php

namespace App\Rules;

use App\Models\TipoPet;
use Illuminate\Contracts\Validation\Rule;

class PetPorteRule implements Rule
{

    private $message = null;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (request()->get('tipo_pet') == TipoPet::whereNome('Cachorro')->first()->id && request()->get('porte') == '')
        {
            $this->message = "O campo porte é obrigatório";
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
