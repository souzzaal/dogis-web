<?php

namespace App\Rules;

use App\Models\AgendamentoServico;
use App\Models\AgendamentoSituacao;
use App\Models\Perfil;
use App\Models\Pessoa;
use App\Models\Pet;
use App\Models\Servico;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class AgendamentoServicoRule implements Rule
{

    private $message;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $data = request()->all();
        // dump($data);
        
        $horarios = [];
        foreach($data['servico'] as $key => $servico) {
            // dd([!isset($data['funcionario'][$key]) , !isset($data['horario'][$key]) , !isset($data['situacao'][$key])]);
            // Verificando se para cada serviço foi informado o funcionário, horário e situação
            if(!isset($data['funcionario'][$key]) || !isset($data['horario'][$key]) || !isset($data['situacao'][$key])) {
                $this->message = 'Para cada serviço deve ser informado o funcionário, o horário e a situação';
                return false;
            }

            $servico = Servico::find($data['servico'][$key]);
            $funcionario = Pessoa::find($data['funcionario'][$key]);

            // verificando se o funcionário pode atender o serviço
            $podeRealizarServico = \DB::table('pessoas_perfis')
            ->where('id_pessoa', $data['funcionario'][$key])
            ->where('id_perfil', $servico->id_perfil)
            ->first();

            if(!$podeRealizarServico) {
                $this->message = 'O funcionário ' . $funcionario->nome . ' não pode atender o serviço ' . $servico->nome;
                return false;
            }

            // verificando se o veterinario atende o tipo de pet
            $pet = Pet::find($data['pet']);
            $perfilVeterinario = Perfil::where('nome', 'Veterinário')->first();
            if ($servico->id_perfil == $perfilVeterinario->id) {
                
                $podeAtenderPet = \DB::table('pessoas_tipos_pets')
                ->where('id_pessoa', $funcionario->id)
                ->where('id_tipo_pet', $pet->id_tipo_pet)
                ->first();

                if(!$podeAtenderPet) {
                    $this->message = 'O veterinário ' . $funcionario->nome . ' não é habilitado para atender pets do tipo ' . $pet->tipo->nome;
                    return false;
                }
            }

            // verificando se o funcionário tem disponibilidade
            $horario_inicial = Carbon::createFromFormat('Y-m-d H:i', $data['data'].' '.$data['horario'][$key]);

            if(request()->IsMethod('post')) {
                if($horario_inicial < Carbon::now()) {
                    $this->message = 'Não é possível agendar para datas passadas';
                    return false;
                }
            }

            $situacao = AgendamentoSituacao::find($data['situacao'][$key]);
            if($horario_inicial >= Carbon::now()) {
                if($situacao->id != AgendamentoSituacao::where('nome','Agendado')->first()->id) {
                    $this->message = 'A situação "' . $situacao->nome .'" é inválida para agendamento futuro';
                    return false;
                }
            }

            $horarios[] = [
                $horario_inicial->format('Y-m-d H:i:s'),
                $horario_inicial,
                $servico->id_unidade_tempo  == 1 ? $servico->tempo_estimado * 60:$servico->tempo_estimado,
                $servico->nome
            ];

            $horario_limite_inferior = Carbon::createFromFormat('Y-m-d H:i', $data['data'].' '.$data['horario'][$key]);
            $horario_limite_superior = Carbon::createFromFormat('Y-m-d H:i', $data['data'].' '.$data['horario'][$key]);
            if($servico->id_unidade_tempo == 1) {
                $horario_limite_inferior->modify('-'.$servico->tempo_estimado. ' hour');
                $horario_limite_superior->modify('+'.$servico->tempo_estimado. ' hour');
            } else {
                $horario_limite_inferior->modify('-'.$servico->tempo_estimado. ' minute');
                $horario_limite_superior->modify('+'.$servico->tempo_estimado. ' minute');
            }

            $agendamentoAnterior = AgendamentoServico::where('id_pessoa', $funcionario->id)
                ->where('data_inicio', '>=', $horario_limite_inferior->format('Y-m-d H:i:s'))
                 ->where('data_inicio', '<=', $horario_inicial->format('Y-m-d H:i:s'));

            if(request()->isMethod('PUT')) {
                $agendamentoAnterior->where('id_agendamento', '<>', request()->route('agendamento'));
            }     
            $agendamentoAnterior = $agendamentoAnterior->count();

            $agendamentoPosterior = AgendamentoServico::where('id_pessoa', $funcionario->id)
                ->where('data_inicio', '>=', $horario_inicial->format('Y-m-d H:i:s'))
                ->where('data_inicio', '<=', $horario_limite_superior->format('Y-m-d H:i:s'));

            if(request()->isMethod('PUT')) {
                $agendamentoPosterior->where('id_agendamento', '<>', request()->route('agendamento'));
            }     
            $agendamentoPosterior = $agendamentoPosterior->count();

            if($agendamentoAnterior || $agendamentoPosterior) {
                $this->message = 'O funcionário ' . $funcionario->nome . ' não tem disponibilidade para atender às ' . $horario_inicial->format('H:i').'h';
                return false;
            }

            
        }

        usort($horarios, function ($element1, $element2) { 
            $datetime1 = strtotime($element1[0]); 
            $datetime2 = strtotime($element2[0]); 
            return $datetime1 - $datetime2; 
        });  
    
        // Verificando se os servicos respeitam o tempo de execução entre si
        for($i=0; $i<count($horarios)-1; $i++) {
            $diff = $horarios[$i][1]->diffInMinutes($horarios[$i+1][1]);
            if($diff < $horarios[$i][2]) {
                $this->message = "Não há tempo suficiente entre os serviços " . $horarios[$i][3] . "( às " . $horarios[$i][1]->format('H:i') . ")" . " e " . $horarios[$i+1][3] . "( às " . $horarios[$i+1][1]->format('H:i') . ")";
                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
