<?php

namespace App\Providers;

use App\Models\Agendamento;
use App\Models\Pet;
use App\Models\Servico;
use App\Policies\AgendamentoPolicy;
use App\Policies\PetPolicy;
use App\Policies\ServicoPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
        Agendamento::class => AgendamentoPolicy::class,
        Pet::class => PetPolicy::class,
        Servico::class => ServicoPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
