<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Preco extends Model
{
    protected $table = 'precos';

    protected $fillable = [
        'id_produto_servico',
        'id_pessoa',
        'preco',
        'id_promocao',
        'ativo'
    ];

    public function getPrecoAttribute()
    {
        return number_format($this->attributes['preco'], 2, ',', '.');
    }

    public function setPrecoAttribute($value)
    {
        $n = str_replace('.','',$value);
		$n = str_replace(',','.',$n);
		$n = floatval($n);
        $this->attributes['preco'] = number_format($n, 2,'.','');
    }
}
