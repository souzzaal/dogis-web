<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Servico extends Model
{
    
    protected $table = 'servicos';

    protected $primaryKey = 'id_produto_servico';

    public $incrementing = false;

    protected $with = ['produtoServico'];

    protected $fillable = [
        'tempo_estimado',
        'pontos_ganhos',
        'pontos_necessarios',
        'id_perfil',
        'id_unidade_tempo'
    ];

    public function getIdAttribute()
    {
        return $this->id_produto_servico;
    }

    public function getNomeAttribute()
    {
        return $this->produtoServico->nome;
    }

    public function getPrecoAttribute()
    {
        $preco = Preco::where('id_produto_servico', $this->id)
            ->where('ativo', 1)
            ->orderBy('id', 'desc')
            ->first();

        if ($preco) {
            return $preco->preco;
        } else {
            return '0,00';
        }
    }

    /**
     * Carrega automaticamente o relacionamento com produtos_servicos
     */
    public function produtoServico()
    {
        return $this->belongsTo('App\Models\ProdutoServico', 'id_produto_servico', 'id');
    }

    /**
     * Faz o JOIN, quando manualmente chamado, com a tabela de produtos_servicos para facilitar a filtragem e ordenação de dados para listagem
     */
    public function scopeProdutoServico()
    {
        return $this->join('produtos_servicos', 'servicos.id_produto_servico', 'produtos_servicos.id');
    }

    public function perfil()
    {
        return $this->belongsTo('App\Models\Perfil', 'id_perfil', 'id');
    }

    public function unidadeTempo()
    {
        return $this->belongsTo('App\Models\UnidadeTempo', 'id_unidade_tempo', 'id');
    }

    public function scopeComNome($query, $nome)
    {
        return $query->where('nome', 'like', "%$nome%");
    }

    public function scopeAtivo($query)
    {
        return $query->whereHas('produtoServico', function ($q) {
            $q->where('ativo', 1);
        });
    }
}
