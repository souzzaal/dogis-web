<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Avaliacao extends Model
{
    protected $table = 'avaliacoes';

    protected $fillable = [
        'id_agendamento_servico',
        'pontuacao'
    ];
   
   protected $dates = ['created_at'];
    
    public function agendamentoServico()
    {
        return $this->belongsTo('App\Models\AgendamentoServico', 'id_agendamento_servico', 'id')
            ->with(['agendamento' => function($query){
                $query->without('servicos');
            }]);
    }
    
    public function scopeDoProfissional($query, $id)
    {
        return $query->whereHas('agendamentoServico', function($q) use ($id) {
            $q->where('id_pessoa', $id);
        });
    }

}
