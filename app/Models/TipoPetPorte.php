<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoPetPorte extends Model
{
    protected $table = 'tipos_pets_portes';

    protected $fillable = [
        'nome',
        'id_tipo_pet'
    ];
}
