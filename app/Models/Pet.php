<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pet extends Model
{
    protected $table = 'pets';

    protected $fillable = [
        'id_pessoa',
        'id_tipo_pet',
        'nome',
        'porte',
        'observacoes',
        'permite_foto',
        'ativo',
    ];

    protected $with = ['dono', 'tipo'];

    public function dono()
    {
        return $this->belongsTo('App\Models\Pessoa', 'id_pessoa', 'id');
    }

    public function tipo()
    {
        return $this->belongsTo('App\Models\TipoPet', 'id_tipo_pet', 'id');
    }

    public function scopeComNome($query, $nome)
    {
        return $query->where('nome', 'like' ,"%$nome%");
    }

    public function scopeAtivo($query, $ativo)
    {
        return $query->where('ativo', $ativo);
    }

}
