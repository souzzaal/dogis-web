<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Pessoa extends Authenticatable
    {
        use Notifiable;
    
        protected $table = 'pessoas';

        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'nome', 'email', 'password',
        ];
    
        /**
         * The attributes that should be hidden for arrays.
         *
         * @var array
         */
        protected $hidden = [
            'password', 'remember_token',
        ];
    
        /**
         * The attributes that should be cast to native types.
         *
         * @var array
         */
        protected $casts = [
            'email_verified_at' => 'datetime',
        ];

        public function perfis()
        {
            return $this->belongsToMany('App\Models\Perfil', 'pessoas_perfis', 'id_pessoa', 'id_perfil');
        }

        /**
         * Query Scope para filtragem de Pessoas por Perfil
         */
        public function scopeComPerfil($query, $perfil)
        {
            return $query->whereHas('perfis', function($q) use ($perfil) {
                $q->where('nome', $perfil);
            });
        }

        /**
         * Verifica se uma Pessoa possui um determinado Perfil
         */
        public function temPerfil($perfil)
        {
            $perfis = $this->perfis->filter(function($value) use ($perfil){
                return $value->nome == $perfil;
            });

            return $perfis->count() ? true : false;
        }

        public function getPrimeiroNomeAttribute()
        {
            $nome = explode(' ', $this->nome);

            return $nome[0];
        }

        public function scopeFuncionarios($query)
        {
            return $query->whereHas('perfis', function($q) {
                $q->whereIn('perfis.id', [
                    Perfil::where('nome', 'Atendente')->first()->id,
                    Perfil::where('nome', 'Veterinário')->first()->id
                ]);
            });
        }

    
}
