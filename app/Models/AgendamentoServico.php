<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class AgendamentoServico extends Model
{
    protected $table = 'agendamentos_servicos';

    protected $fillable = [ 
        'id_agendamento',
        'id_servico',
        'id_pessoa',
        'data_inicio',
        'cliente_notificado',
        'id_agendamento_situacao'
    ];

    protected $dates = ['data_inicio'];

    protected $with = ['produtoServico'];

    public function agendamento()
    {
        return $this->belongsTo('App\Models\Agendamento', 'id_agendamento', 'id');
    } 
    
    public function produtoServico()
    {
        return $this->belongsTo('App\Models\ProdutoServico', 'id_servico', 'id');
    }

    public function getHorarioAttribute()
    {
        $carbon = Carbon::parse($this->attributes['data_inicio']);
        return $carbon->format('H:i');
    }

    public function veterinario()
    {
        return $this->belongsTo('App\Models\Pessoa', 'id_pessoa', 'id');
    }
}
