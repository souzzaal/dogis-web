<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgendamentoSituacao extends Model
{
    protected $table = 'agendamentos_situacoes';
    public $timestamps = false;

    protected $fillable = [
        'nome'
    ];
}

