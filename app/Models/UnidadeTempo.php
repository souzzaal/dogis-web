<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UnidadeTempo extends Model
{
    protected $table = 'unidades_tempo';

    public function getSiglaAttribute()
    {
        if ($this->nome == 'Horas') {
            return 'h';
        } else if ($this->nome == 'Minutos') {
            return 'm';
        }

    }
}
