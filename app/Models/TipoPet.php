<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoPet extends Model
{
    protected $table = 'tipos_pets';

    protected $fillable = [
        'nome'
    ];
}
