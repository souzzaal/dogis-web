<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agendamento extends Model
{
    protected $table = 'agendamentos';

    protected $fillable = [
        'id_pet'
    ];

    protected $with = ['servicos', 'pet'];

    public function servicos()
    {
        return $this->hasMany('App\Models\AgendamentoServico', 'id_agendamento', 'id');
    }

    public function scopeComData($query, $data)
    {
        return $query->whereHas('servicos', function($q) use ($data) {
            $q->whereDate('data_inicio', $data);
        });
    }

    public function pet()
    {
        return $this->belongsTo('App\Models\Pet', 'id_pet', 'id');
    }

}
