<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoriaProdutoServico extends Model
{
    protected $table = 'categorias_produtos_servicos';
}
