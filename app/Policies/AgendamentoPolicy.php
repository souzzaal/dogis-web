<?php

namespace App\Policies;

use App\Models\Agendamento;
use App\Models\Pessoa;
use Illuminate\Auth\Access\HandlesAuthorization;

class AgendamentoPolicy
{

    /**
     * Determine whether the user can view any servicos.
     *
     * @param  \App\Models\Pessoa  $user
     * @return mixed
     */
    public function viewAny(Pessoa $user)
    {   
        // dd($user->email, $user->temPerfil('Atendente'));
        return $user->temPerfil('Atendente');
    }

    /**
     * Determine whether the user can view the servico.
     *
     * @param  \App\Models\Pessoa  $user
     * @param  \App\App\Models\Agendamento  $agendamento
     * @return mixed
     */
    public function view(Pessoa $user, Agendamento $agendamento)
    {
        return $user->temPerfil('Atendente');
    }

    /**
     * Determine whether the user can create servicos.
     *
     * @param  \App\Models\Pessoa  $user
     * @return mixed
     */
    public function create(Pessoa $user)
    {
        return $user->temPerfil('Atendente');
    }

    /**
     * Determine whether the user can update the servico.
     *
     * @param  \App\Models\Pessoa  $user
     * @param  \App\App\Models\Agendamento  $agendamento
     * @return mixed
     */
    public function update(Pessoa $user, Agendamento $agendamento)
    {
        return $user->temPerfil('Atendente');

    }

    /**
     * Determine whether the user can delete the servico.
     *
     * @param  \App\Models\Pessoa  $user
     * @param  \App\App\Models\Agendamento  $agendamento
     * @return mixed
     */
    public function delete(Pessoa $user, Agendamento $agendamento)
    {
        return $user->temPerfil('Atendente');
    }

    /**
     * Determine whether the user can restore the servico.
     *
     * @param  \App\Models\Pessoa  $user
     * @param  \App\App\Models\Agendamento  $agendamento
     * @return mixed
     */
    public function restore(Pessoa $user, Agendamento $agendamento)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the servico.
     *
     * @param  \App\Models\Pessoa  $user
     * @param  \App\App\Models\Agendamento  $agendamento
     * @return mixed
     */
    public function forceDelete(Pessoa $user, Agendamento $agendamento)
    {
        //
    }
}
