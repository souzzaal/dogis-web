<?php

namespace App\Policies;

use App\Models\Pessoa;
use App\Models\Pet;
use Illuminate\Auth\Access\HandlesAuthorization;

class PetPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any servicos.
     *
     * @param  \App\Models\Pessoa  $user
     * @return mixed
     */
    public function viewAny(Pessoa $user)
    {
        return $user->temPerfil('Atendente');
    }

    /**
     * Determine whether the user can view the servico.
     *
     * @param  \App\Models\Pessoa  $user
     * @param  \App\Models\Pet  $pet
     * @return mixed
     */
    public function view(Pessoa $user, Pet  $pet)
    {
        return $user->temPerfil('Atendente');
    }

    /**
     * Determine whether the user can create servicos.
     *
     * @param  \App\Models\Pessoa  $user
     * @return mixed
     */
    public function create(Pessoa $user)
    {
        return $user->temPerfil('Atendente');
    }

    /**
     * Determine whether the user can update the servico.
     *
     * @param  \App\Models\Pessoa  $user
     * @param  \App\Models\Pet  $pet
     * @return mixed
     */
    public function update(Pessoa $user, Pet  $pet)
    {
        return $user->temPerfil('Atendente');

    }

    /**
     * Determine whether the user can delete the servico.
     *
     * @param  \App\Models\Pessoa  $user
     * @param  \App\Models\Pet  $pet
     * @return mixed
     */
    public function delete(Pessoa $user, Pet  $pet)
    {
        return $user->temPerfil('Atendente');
    }

    /**
     * Determine whether the user can restore the servico.
     *
     * @param  \App\Models\Pessoa  $user
     * @param  \App\Models\Pet  $pet
     * @return mixed
     */
    public function restore(Pessoa $user, Pet  $pet)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the servico.
     *
     * @param  \App\Models\Pessoa  $user
     * @param  \App\Models\Pet  $pet
     * @return mixed
     */
    public function forceDelete(Pessoa $user, Pet  $pet)
    {
        //
    }
}
