<?php

namespace App\Policies;

use App\Models\Pessoa;
use App\Models\Servico;
use Illuminate\Auth\Access\HandlesAuthorization;

class ServicoPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any servicos.
     *
     * @param  \App\Models\Pessoa  $user
     * @return mixed
     */
    public function viewAny(Pessoa $user)
    {
        return $user->temPerfil('Administrador');
    }

    /**
     * Determine whether the user can view the servico.
     *
     * @param  \App\Models\Pessoa  $user
     * @param  \App\Models\Servico  $servico
     * @return mixed
     */
    public function view(Pessoa $user, Servico $servico)
    {
        return $user->temPerfil('Administrador');
    }

    /**
     * Determine whether the user can create servicos.
     *
     * @param  \App\Models\Pessoa  $user
     * @return mixed
     */
    public function create(Pessoa $user)
    {
        return $user->temPerfil('Administrador');
    }

    /**
     * Determine whether the user can update the servico.
     *
     * @param  \App\Models\Pessoa  $user
     * @param  \App\Models\Servico  $servico
     * @return mixed
     */
    public function update(Pessoa $user, Servico $servico)
    {
        // dd($user, $servico);
        return $user->temPerfil('Administrador');

    }

    /**
     * Determine whether the user can delete the servico.
     *
     * @param  \App\Models\Pessoa  $user
     * @param  \App\Models\Servico  $servico
     * @return mixed
     */
    public function delete(Pessoa $user, Servico $servico)
    {
        return $user->temPerfil('Administrador');
    }

    /**
     * Determine whether the user can restore the servico.
     *
     * @param  \App\Models\Pessoa  $user
     * @param  \App\Models\Servico  $servico
     * @return mixed
     */
    public function restore(Pessoa $user, Servico $servico)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the servico.
     *
     * @param  \App\Models\Pessoa  $user
     * @param  \App\Models\Servico  $servico
     * @return mixed
     */
    public function forceDelete(Pessoa $user, Servico $servico)
    {
        //
    }
}
