<?php

namespace App\Repositories;

use App\Models\Agendamento;
use App\Models\AgendamentoServico;
use App\Models\AgendamentoSituacao;
use App\Models\Pessoa;
use App\Models\Pet;
use App\Models\Servico;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class AgendamentoRepository
{
    public function index(Request $request)
    {
        $orm = Agendamento::orderBy('id');

        if ($request->filled('data')) {

            try {
                $carbon = Carbon::parse($request->get('data'))->setTimezone('America/Sao_Paulo');
            } catch (\Exception $e) {
                Session::flash('message', ['type' => 'danger', 'message' => 'Data inválida']);
                return redirect()->back()->withInput();
            }

            $data = $request->get('data');
        } else {
            $data = date('Y-m-d');
        }
        $orm->comData($data);

        $model = $orm->paginate(10);

        return view('agendamentos.index', ['agendamentos' => $model]);
    }

    public function create()
    {
        $pets = Pet::ativo(1)->get();
        $situacoes = AgendamentoSituacao::all();
        $funcionarios = Pessoa::funcionarios()->get();
        $servicos = Servico::ativo()->get();

        return view('agendamentos.create', [
            'pets' => $pets,
            'situacoes' => $situacoes,
            'funcionarios' => $funcionarios,
            'servicos' => $servicos
        ]);
    }

    public function store(Request $request)
    {
        
        try {
            DB::beginTransaction();

            $data = $request->all();
            $agendamento = Agendamento::create([
                'id_pet' => $data['pet']
            ]);

            foreach ($data['servico'] as $key => $idServico) {
                $servico = Servico::find($idServico);

                $horario_inicial = Carbon::createFromFormat('Y-m-d H:i', $data['data'] . ' ' . $data['horario'][$key]);
                $horario_final = Carbon::createFromFormat('Y-m-d H:i', $data['data'] . ' ' . $data['horario'][$key]);

                if ($servico->id_unidade_tempo == 1) {
                    $horario_final->modify('+' . $servico->tempo_estimado . ' hour');
                } else {
                    $horario_final->modify('+' . $servico->tempo_estimado . ' minute');
                }

                AgendamentoServico::create([
                    'id_agendamento' => $agendamento->id,
                    'id_servico' => $idServico,
                    'id_pessoa' => $data['funcionario'][$key],
                    'id_agendamento_situacao' => $data['situacao'][$key],
                    'data_inicio' => $horario_inicial->format('Y-m-d H:i:00'),
                    'cliente_notificado' => 0,
                ]);
            }
            DB::commit();
            Session::flash('message', ['type' => 'success', 'message' => 'Agendamento cadastrado com sucesso.']);
        } catch (\Exception $exception) {
            DB::rollBack();
            Session::flash('message', ['type' => 'danger', 'message' => 'Falha ao cadastrar agendameto.']);
        }
    }
    
    public function edit(Agendamento $agendamento)
    {
        $pets = Pet::ativo(1)->get();
        $situacoes = AgendamentoSituacao::all();
        $funcionarios = Pessoa::funcionarios()->get();
        $servicos = Servico::ativo()->get();

        return view('agendamentos.edit', [
            'agendamento' => $agendamento,
            'pets' => $pets,
            'situacoes' => $situacoes,
            'funcionarios' => $funcionarios,
            'servicos' => $servicos
        ]);
    }

    public function update(Request $request, Agendamento $agendamento)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            $agendamento->update([
                'id_pet' => $data['pet']
            ]);

            $agendamento->servicos()->delete();
            
            foreach ($data['servico'] as $key => $idServico) {
                
                $servico = Servico::find($idServico);

                $horario_inicial = Carbon::createFromFormat('Y-m-d H:i', $data['data'] . ' ' . $data['horario'][$key]);
                $horario_final = Carbon::createFromFormat('Y-m-d H:i', $data['data'] . ' ' . $data['horario'][$key]);

                if ($servico->id_unidade_tempo == 1) {
                    $horario_final->modify('+' . $servico->tempo_estimado . ' hour');
                } else {
                    $horario_final->modify('+' . $servico->tempo_estimado . ' minute');
                }

                AgendamentoServico::create([
                    'id_agendamento' => $agendamento->id,
                    'id_servico' => $idServico,
                    'id_pessoa' => $data['funcionario'][$key],
                    'id_agendamento_situacao' => $data['situacao'][$key],
                    'data_inicio' => $horario_inicial->format('Y-m-d H:i:00'),
                    'cliente_notificado' => 0
                ]);
            }
            DB::commit();
            Session::flash('message', ['type' => 'success', 'message' => 'Agendamento(s) atualizado(s) com sucesso.']);
        } catch (\Exception $exception) {
            DB::rollBack();
            dd($exception);
            Session::flash('message', ['type' => 'danger', 'message' => 'Falha ao atualizar agendameto(s).']);
        }
    }
    
    public function destroy(Agendamento $agendamento)
    {
        try {
            DB::beginTransaction();
            $agendamento->servicos()->delete();
            $agendamento->delete();
            DB::commit();
            Session::flash('message', ['type'=>'success', 'message'=>'Agendamento excluído com sucesso.']);
        } catch (\Exception $exception) {
            DB::rollBack();
            Session::flash('message', ['type'=>'danger', 'message'=>'Falha ao excluir agendamento.']);
        }
    }
}
