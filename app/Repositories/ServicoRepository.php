<?php

namespace App\Repositories;

use App\Models\CategoriaProdutoServico;
use App\Models\Perfil;
use App\Models\Preco;
use App\Models\ProdutoServico;
use App\Models\Servico;
use App\Models\UnidadeTempo;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ServicoRepository
{
        public function index(Request $request) 
        {
            $orm = Servico::with(['perfil', 'unidadeTempo'])->produtoServico()
                ->orderBy('produtos_servicos.nome');
            
            if ($request->filled('nome')) {
                $orm->comNome($request->get('nome'));
            }

            $model = $orm->paginate(10);

            return view('servicos.index',['servicos' => $model]);
        }
    
    public function show($key) 
    {

    }

    public function create()
    {
        $unidadeTempo = UnidadeTempo::select('id', 'nome')->get();        
        $perfis = Perfil::select('id', 'nome')->where('id','>', 2)->get();
        
        return view('servicos.create',['unidades_tempo'=>$unidadeTempo, 'perfis'=>$perfis]);
    }
    
    public function store(Request $request) {
        
        $data = $request->all();

        try {
            DB::beginTransaction();
            $produtoServico = new ProdutoServico();
            $produtoServico->nome = $data['nome'];
            $produtoServico->ativo = isset($data['ativo']) ? 1 : 0;
            $produtoServico->id_categoria_produto_servico = CategoriaProdutoServico::whereNome('Serviço')->first()->id;
            $produtoServico->save();

            $servico = new Servico();
            $data['id_perfil'] = $data['atendido_por'];
            $data['id_unidade_tempo'] = $data['unidade_tempo'];
            $servico->fill($data);
            $servico->id_produto_servico = $produtoServico->id;
            $servico->save();
            $preco = Preco::create([
                'id_produto_servico' => $produtoServico->id,
                'id_pessoa' => $request->user()->id,
                'preco' => $data['preco'],
                'ativo' => 1
            ]); 
            DB::commit();
            Session::flash('message', ['type'=>'success', 'message'=>'Serviço cadastrado com sucesso.']);
            
        } catch (\Exception $exception) {
            DB::rollBack();
            Session::flash('message', ['type'=>'danger', 'message'=>'Falha ao cadastrar serviço.']);
        }
        
    }

    public function edit($id)
    {
        $servico = Servico::find($id);
        $unidadeTempo = UnidadeTempo::select('id', 'nome')->get();        
        $perfis = Perfil::select('id', 'nome')->where('id','>', 2)->get();
        
        return view('servicos.edit',['servico'=>$servico,'unidades_tempo'=>$unidadeTempo, 'perfis'=>$perfis]);
    }
    
    public function update(Request $request, Servico $servico) 
    {
        $data = $request->all();
        
        try {
            DB::beginTransaction();
            $servico->produtoServico->nome = $data['nome'];
            $servico->produtoServico->ativo = isset($data['ativo']) ? 1 : 0;
            $servico->produtoServico->save();
            
            $data['id_perfil'] = $data['atendido_por'];
            $data['id_unidade_tempo'] = $data['unidade_tempo'];
            $servico->update($data);

            $preco = Preco::whereAtivo(1)->whereIdProdutoServico($servico->id)->orderBy('id', 'desc')->first();
            if ($preco->preco != $data['preco']) {
                $preco->update(['ativo'=>0]);
                $novoPreco = Preco::create([
                    'id_produto_servico' => $servico->id,
                    'id_pessoa' => $request->user()->id,
                    'preco' => $data['preco'],
                    'ativo' => 1
                ]); 
            }
            DB::commit();
            Session::flash('message', ['type'=>'success', 'message'=>'Serviço atualizado com sucesso.']);
        } catch (\Exception $exception) {
            DB::rollBack();
            Session::flash('message', ['type'=>'danger', 'message'=>'Falha ao atualizar serviço.']);
        }
    }
    
    public function destroy($servico) 
    {
        try {
            $servico->delete();
            Session::flash('message', ['type'=>'success', 'message'=>'Serviço excluído com sucesso.']);
        } catch (\Exception $exception) {
            Session::flash('message', ['type'=>'danger', 'message'=>'Falha ao excluir serviço.']);
        }
    }   
}