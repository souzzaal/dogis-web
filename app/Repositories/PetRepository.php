<?php

namespace App\Repositories;

use App\Models\AgendamentoServico;
use App\Models\Pessoa;
use App\Models\Pet;
use App\Models\TipoPet;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;

class PetRepository
{
    public function index(Request $request)
    {
        $orm = Pet::orderBy('pets.nome');

        if ($request->filled('nome')) {
            $orm->comNome($request->get('nome'));
        }

        $model = $orm->paginate(10);

        return view('pets.index', ['pets' => $model]);
    }

    public function create()
    {
        $tipos = TipoPet::select('id', 'nome')->get();
        $clientes = Pessoa::select('id', 'nome')->get();

        return view('pets.create', ['tipos' => $tipos, 'clientes' => $clientes]);
    }

    public function store(Request $request)
    {
        $data = $request->all();

        try {
            $data['id_pessoa'] = $data['cliente'];
            $data['id_tipo_pet'] = $data['tipo_pet'];
            $data['ativo'] = isset($data['ativo']) ? 1 : 0;
            $data['permite_foto'] = isset($data['permite_foto']) ? 1 : 0;
            Pet::create($data);

            Session::flash('message', ['type' => 'success', 'message' => 'Pet cadastrado com sucesso.']);
        } catch (\Exception $exception) {
            Session::flash('message', ['type' => 'danger', 'message' => 'Falha ao cadastrar pet.']);
        }
    }

    public function edit(Pet $pet)
    {
        $tipos = TipoPet::select('id', 'nome')->get();
        $clientes = Pessoa::select('id', 'nome')->get();

        return view('pets.edit', ['pet' => $pet, 'tipos' => $tipos, 'clientes' => $clientes]);
    }

    public function update(Request $request, Pet $pet)
    { 
        $data = $request->all();

        try {
            $data['id_pessoa'] = $data['cliente'];
            $data['id_tipo_pet'] = $data['tipo_pet'];
            $data['ativo'] = isset($data['ativo']) ? 1 : 0;
            $data['permite_foto'] = isset($data['permite_foto']) ? 1 : 0;
            $pet->update($data);

            Session::flash('message', ['type' => 'success', 'message' => 'Pet atualizado com sucesso.']);
        } catch (\Exception $exception) {
            Session::flash('message', ['type' => 'danger', 'message' => 'Falha ao atualizar pet.']);
        }
    }

    public function destroy(Pet $pet)
    {
        try {
            $pet->delete();
            Session::flash('message', ['type'=>'success', 'message'=>'Pet excluído com sucesso.']);
        } catch (\Exception $exception) {
            Session::flash('message', ['type'=>'danger', 'message'=>'Falha ao excluir pet.']);
        }
    }
}
