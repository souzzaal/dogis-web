<?php

namespace App\Repositories;

use App\Models\Avaliacao;
use App\Models\Pessoa; 
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RelatorioAvaliacaoRepository
{
    
    public function index(Request $request)
    {

        $model = collect();
        $showAlert = false;
        if ($request->filled('profissional') && $request->filled('data_inicio') && $request->filled('data_final')) {
            $model = Avaliacao::with('agendamentoServico')
                ->whereHas('agendamentoServico', function($q) use ($request) {
                    $q->where('id_pessoa', $request->get('profissional'))
                        ->where('data_inicio','>=', $request->get('data_inicio') . ' 00:00:00')
                        ->where('data_inicio','<=', $request->get('data_final') . ' 23:59:59');
                })
                ->get()
                ;
            
            $showAlert = true;
        }

        $funcionarios = Pessoa::funcionarios()->get();

        return view('relatorios.avaliacoes.index', ['avaliacoes' => $model, 'funcionarios' => $funcionarios, 'showAlert' => $showAlert]);
    }
}