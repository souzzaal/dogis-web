<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProdutosServicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produtos_servicos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome', 255);
            $table->boolean('ativo');
            $table->unsignedInteger('id_categoria_produto_servico');
            $table->timestamps();

            $table->foreign('id_categoria_produto_servico')->references('id')->on('categorias_produtos_servicos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produtos_servicos');
    }
}
