<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrecosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('precos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('id_produto_servico');
            $table->unsignedInteger('id_pessoa');
            $table->double('preco', 8, 2);
            $table->unsignedInteger('id_promocao')->nullable();
            $table->boolean('ativo');
            $table->timestamps();

            $table->foreign('id_produto_servico')->references('id')->on('produtos_servicos');
            $table->foreign('id_pessoa')->references('id')->on('pessoas');
            $table->foreign('id_promocao')->references('id')->on('promocoes');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('precos');
    }
}
