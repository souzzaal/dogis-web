<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePessoasTiposPetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pessoas_tipos_pets', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_pessoa');
            $table->unsignedInteger('id_tipo_pet');
            $table->timestamps();

            $table->foreign('id_pessoa')->references('id')->on('pessoas');
            $table->foreign('id_tipo_pet')->references('id')->on('pessoas_tipos_pets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pessoas_tipos_pets');
    }
}
