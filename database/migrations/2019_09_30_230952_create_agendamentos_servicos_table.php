<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgendamentosServicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agendamentos_servicos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_agendamento');
            $table->unsignedBigInteger('id_servico');
            $table->unsignedBigInteger('id_pessoa');
            $table->dateTime('data_inicio');
            $table->boolean('cliente_notificado');
            $table->unsignedInteger('id_agendamento_situacao');
            $table->timestamps();

            $table->foreign('id_agendamento')->references('id')->on('agendamentos');
            // $table->foreign('id_servico')->references('id_produto_servico')->on('servicos');
            $table->foreign('id_pessoa')->references('id')->on('pessoas');
            $table->foreign('id_agendamento_situacao')->references('id')->on('agendamentos_situacoes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agendamentos_servicos');
    }
}
