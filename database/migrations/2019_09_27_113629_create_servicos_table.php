<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicos', function (Blueprint $table) {
            $table->unsignedInteger('id_produto_servico');
            $table->unsignedInteger('tempo_estimado');
            $table->unsignedInteger('pontos_ganhos');
            $table->unsignedInteger('pontos_necessarios');
            $table->unsignedInteger('id_perfil');
            $table->unsignedInteger('id_unidade_tempo');
            $table->timestamps();

            $table->foreign('id_perfil')->references('id')->on('perfis');
            $table->foreign('id_unidade_tempo')->references('id')->on('unidades_tempo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicos');
    }
}
