<?php

use App\Models\TipoPet;
use Illuminate\Database\Seeder;

class TiposPetsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (TipoPet::count() == 0) {
            TipoPet::create(['nome'=>'Cachorro']);   
            TipoPet::create(['nome'=>'Gato']);
            TipoPet::create(['nome'=>'Pássaro']);
            TipoPet::create(['nome'=>'Peixe']);
        }
    }
}
