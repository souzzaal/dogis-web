<?php

use App\Models\Perfil;
use Illuminate\Database\Seeder;

class PerfisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(Perfil::count() == 0) {
            Perfil::create(['nome' => 'Administrador']);
            Perfil::create(['nome' => 'Cliente']);
            Perfil::create(['nome' => 'Atendente']);
            Perfil::create(['nome' => 'Veterinário']);  
        }
    }
}
