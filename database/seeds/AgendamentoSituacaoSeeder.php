<?php

use App\Models\AgendamentoSituacao;
use Illuminate\Database\Seeder;

class AgendamentoSituacaoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (AgendamentoSituacao::count() ==0) {
            AgendamentoSituacao::create(['nome' => 'Agendado']);
            AgendamentoSituacao::create(['nome' => 'Ausente']);
            AgendamentoSituacao::create(['nome' => 'Realizado']);
        }
    }
}
