<?php

use App\Models\Perfil;
use App\Models\Pessoa;
use App\Models\TipoPet;
use Illuminate\Database\Seeder;

class PessoasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (App::environment('local')) {
            
            if(Pessoa::count() ==0) {
            
                $perfis = Perfil::all();
                
                foreach($perfis as $perfil) {
                    $pessoa = factory(Pessoa::class)->create([
                        'nome' => $perfil->nome,
                        'email' => strtolower($perfil->nome) . '@mail.com'
                    ]);
                    $pessoa->perfis()->save($perfil);
                }
    
                $perfilVeterinario = Perfil::where('nome', 'Veterinário')->first();
                foreach(TipoPet::all() as $tipoPet) {
                    $pessoa = factory(Pessoa::class)->create([
                        'nome' => 'Dr. ' . $tipoPet->nome,
                        'email' => 'dr' . strtolower($tipoPet->nome) . '@mail.com'
                    ]);
                    $pessoa->perfis()->save($perfilVeterinario);
                    DB::table('pessoas_tipos_pets')->insert([
                        'id_pessoa' => $pessoa->id,
                        'id_tipo_pet' => $tipoPet->id
                    ]);
                }
            }
        }
    }
}
