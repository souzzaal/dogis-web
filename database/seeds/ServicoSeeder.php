<?php

use App\Models\Perfil;
use App\Models\Servico;
use Illuminate\Database\Seeder;

class ServicoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (App::environment('local')) {
        
            if(Servico::count()==0) {
                $s = factory(Servico::class)->create([
                    'id_perfil' => Perfil::where('nome', 'Atendente')->first()->id
                ]);
                
                $s->produtoServico->nome = 'Banho';
                $s->produtoServico->save();
                
                $s = factory(Servico::class)->create([
                    'id_perfil' => Perfil::where('nome', 'Veterinário')->first()->id
                ]);
                $s->produtoServico->nome = 'Vacina';
                $s->produtoServico->save();
            }

        }
    }
    
}
