<?php

use App\Models\Pet;
use App\Models\TipoPet;
use Illuminate\Database\Seeder;

class PetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (App::environment('local')) {
            
            if(Pet::count() == 0) {
               foreach(TipoPet::all() as $tipoPet) {
                   factory(Pet::class)->create([
                    'nome' => $tipoPet->nome
                   ]);
               }
           }

        }
    }
}
