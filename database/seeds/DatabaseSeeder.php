<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        Artisan::call('vendor:publish --tag=laravel-pt-br-localization');

        $this->call(PerfisSeeder::class);
        $this->call(TiposPetsSeeder::class);
        $this->call(PessoasSeeder::class);
        $this->call(CategoriaProdutoServicoSeeder::class);
        $this->call(UnidadeTempoSeeder::class);
        $this->call(AgendamentoSituacaoSeeder::class);
        $this->call(PetSeeder::class);
        $this->call(ServicoSeeder::class);
    }
}
