<?php

use App\Models\UnidadeTempo;
use Illuminate\Database\Seeder;

class UnidadeTempoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(UnidadeTempo::count() == 0) {
            UnidadeTempo::create(['nome' => 'Horas']);
            UnidadeTempo::create(['nome' => 'Minutos']);
        }
    }
}
