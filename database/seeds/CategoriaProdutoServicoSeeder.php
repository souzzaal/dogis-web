<?php

use App\Models\CategoriaProdutoServico;
use Illuminate\Database\Seeder;

class CategoriaProdutoServicoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (CategoriaProdutoServico::count() ==0) {
            CategoriaProdutoServico::create(['nome' => 'Serviço']);
            CategoriaProdutoServico::create(['nome' => 'Produto']);
        }
    }
}
