<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Agendamento;
use Faker\Generator as Faker;

$factory->define(Agendamento::class, function (Faker $faker) {
    return [
        'id_pet' => factory(App\Models\Pet::class)->create()
    ];
});
