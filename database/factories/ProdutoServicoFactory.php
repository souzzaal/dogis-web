<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\CategoriaProdutoServico;
use App\Models\ProdutoServico;
use Faker\Generator as Faker;

$factory->define(ProdutoServico::class, function (Faker $faker) {
    return [
        'nome' => $faker->word .' ' . date('YmdHis'),
        'ativo' => 1,
        'id_categoria_produto_servico' => CategoriaProdutoServico::first()->id
    ];
});
