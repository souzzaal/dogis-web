<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Agendamento;
use App\Models\AgendamentoServico;
use App\Models\AgendamentoSituacao;
use App\Models\Pessoa;
use App\Models\Servico;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(AgendamentoServico::class, function (Faker $faker) {
    
    $data = $faker->dateTimeBetween($startDate = '+1000 years', $endDate = '+3000 years');
    
    return [ 
        'id_agendamento' => factory(Agendamento::class)->create()->id,
        'id_servico' => factory(Servico::class)->create()->id_produto_servico,
        'id_pessoa' => Pessoa::comPerfil('Atendente')->first()->id,
        'data_inicio' => $data->format('Y-m-d H:i:s'),
        // 'data_final'=> $data->modify('+1 hour')->format('Y-m-d H:i:s')
        'cliente_notificado' => 0,
        'id_agendamento_situacao' => AgendamentoSituacao::first()->id
    ];
});
