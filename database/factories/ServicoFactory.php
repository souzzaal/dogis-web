<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\CategoriaProdutoServico;
use App\Models\Perfil;
use App\Models\Preco;
use App\Models\Servico;
use App\Models\ProdutoServico;
use App\Models\UnidadeTempo;
use Faker\Generator as Faker;

$factory->define(Servico::class, function (Faker $faker) {
    
    $produto_servico = factory(ProdutoServico::class)->create([
        'id_categoria_produto_servico' => CategoriaProdutoServico::whereNome('Serviço')->first()->id
    ]);
    
    $preco = factory(Preco::class)->create([
        'id_produto_servico' => $produto_servico,
    ]);

    return [
        'id_produto_servico' => $produto_servico,
        'tempo_estimado' => $faker->numberBetween(0, 60),
        'pontos_ganhos' => $faker->numberBetween(10, 100),
        'pontos_necessarios' => $faker->numberBetween(50, 200),
        'id_perfil' => Perfil::first()->id,
        'id_unidade_tempo' => UnidadeTempo::first()->id
    ];
});
