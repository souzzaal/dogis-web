<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\TipoPet;
use Faker\Generator as Faker;

$factory->define(TipoPet::class, function (Faker $faker) {
    return [
        'nome'=> $faker->unique()->word
    ];
});
