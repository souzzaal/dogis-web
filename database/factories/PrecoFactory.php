<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Pessoa;
use App\Models\Preco;
use App\Models\ProdutoServico;
use Faker\Generator as Faker;

$factory->define(Preco::class, function (Faker $faker) {
    return [
        'id_produto_servico' => ProdutoServico::first(),
        'id_pessoa' => Pessoa::first(),
        'preco' => 10,
        'ativo' => 1
    ];
});
