<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Pessoa;
use App\Models\Pet;
use App\Models\TipoPet;
use Faker\Generator as Faker;

$factory->define(Pet::class, function (Faker $faker) {
    return [
        'id_pessoa' => Pessoa::comPerfil('Cliente')->get()->random(),
        'id_tipo_pet' => TipoPet::all()->random(),
        'nome' => $faker->word,
        'porte' => '',
        'observacoes' => '',
        'permite_foto' => 1,
        'ativo' => 1,
    ];
});
