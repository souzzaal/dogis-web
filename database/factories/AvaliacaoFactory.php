<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Avaliacao;
use App\Models\AgendamentoServico;
use Faker\Generator as Faker;

$factory->define(Avaliacao::class, function (Faker $faker) {
    return [
        'id_agendamento_servico' => factory(AgendamentoServico::class)->create()->id,
        'pontuacao' => $faker->numberBetween(0, 5)
    ];
});
