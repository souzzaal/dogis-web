<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register'=>false, 'reset'=>false]);

Route::group(['middleware' => ['auth']], function () {
    Route::resource('/agendamentos', 'AgendamentoController');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('/pets', 'PetController');
    Route::resource('/servicos', 'ServicoController');
    
    Route::prefix('relatorios')->group(function(){
        Route::get('/avaliacoes', 'RelatorioAvaliacoesController@index')->name('relatorio_avaliacoes');
    });
    
    
});
