<?php

namespace Tests\Browser;

use App\Models\AgendamentoServico;
use App\Models\Avaliacao;
use App\Models\Perfil;
use App\Models\Pessoa;
use App\Models\Servico;
use Carbon\Carbon;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RelatorioAvaliacaoTest extends DuskTestCase
{
    public function testAbreTelaInicial()
    {
        $this->browse(function (Browser $browser) {
            $browser
            ->loginAs(Pessoa::comPerfil('Administrador')->first())
            ->visit('/relatorios/avaliacoes')
            ->assertSee('Relatório de avaliações por período')
            ->assertVisible('#profissional')
            ->assertVisible('#data_inicio')
            ->assertVisible('#data_final')
            ->assertVisible('#filtrar')
            ->assertDontSee('Nenhum resultado encontrado')
            ;
        });
    }

    public function testFiltraComPeriodoInvalido()
    {
        $this->browse(function (Browser $browser) {
            $browser
            ->loginAs(Pessoa::comPerfil('Administrador')->first())
            ->visit('/relatorios/avaliacoes')
            ->type('data_inicio', date('d/m/Y'))
            ->type('data_final', '01/01/2001')
            ->click('#filtrar')
            ->waitForText('Relatório de avaliações por período')
            ->assertVisible('#profissional')
            ->assertVisible('#data_inicio')
            ->assertVisible('#data_final')
            ->assertVisible('#filtrar')
            ->assertSee('Nenhum registro encontrado')
            ;
        });
    }

    public function testProfissionalSemAvaliacaoNoPeriodo()
    {
        $profissional = Pessoa::funcionarios()->get()->random(1)->first();

        $data = Carbon::now()->modify('-1000 year');

        $this->browse(function (Browser $browser) use ($profissional, $data) {
            $browser
            ->loginAs(Pessoa::comPerfil('Administrador')->first())
            ->visit('/relatorios/avaliacoes')
            ->value('form #profissional', $profissional->id)
            ->type('data_inicio', $data->format('d/m/Y'))
            ->type('data_final', $data->format('d/m/Y'))
            ->click('#filtrar')
            ->waitForText('Relatório de avaliações por período')
            ->assertVisible('#profissional')
            ->assertVisible('#data_inicio')
            ->assertVisible('#data_final')
            ->assertVisible('#filtrar')
            ->assertSee('Nenhum registro encontrado')
            ;
        });
    }

    public function testProfissionalComAvaliacaoNoPeriodo()
    {
        $profissional = factory(Pessoa::class)->create();
        $perfil = Perfil::whereIn('nome', ['Atendente', 'Veterinário'])
            ->get()
            ->random(1)
            ->first();

        \DB::table('pessoas_perfis')->insert([
            'id_pessoa' => $profissional->id,
            'id_perfil' => $perfil->id
        ]);

        $data = Carbon::now()->modify('-30 day');
        
        $avaliacao = factory(Avaliacao::class)->create([
            'id_agendamento_servico' => factory(AgendamentoServico::class)->create([
                'id_pessoa' => $profissional->id,
                'data_inicio' => $data->format('Y-m-d H:i:s'),
                'id_servico' => Servico::where('id_perfil', $perfil->id)->get()->random()->first()->id
            ])
        ]);

        $avaliacao->first()->load('agendamentoServico');

        $this->browse(function (Browser $browser) use ($profissional, $data) {
            $browser
            ->loginAs(Pessoa::comPerfil('Administrador')->first())
            ->visit('/relatorios/avaliacoes')
            ->value('form #profissional', $profissional->id)
            ->type('data_inicio', $data->format('d/m/Y'))
            ->type('data_final', $data->format('d/m/Y'))
            ->click('#filtrar')
            ->waitForText('Relatório de avaliações por período')
            ->assertVisible('#profissional')
            ->assertVisible('#data_inicio')
            ->assertVisible('#data_final')
            ->assertVisible('#filtrar')
            ->assertSee('#')
            ->assertSee('Data')
            ->assertSee('Pet')
            ->assertSee('Tipo')
            ->assertSee('Cliente')
            ->assertSee('Serviço')
            ->assertSee('Pontuação')
            ->assertSee('Média')
            ->assertDontSee('Nenhum registro encontrado')
            ;
        });
    }

}
