<?php

namespace Tests\Browser;

use App\Models\Agendamento;
use App\Models\Pessoa;
use App\Models\Pet;
use App\Models\TipoPet;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class PetTest extends DuskTestCase
{
  
    public function testListagemDePetsParaAtendente()
    {
        $this->browse(function (Browser $browser) {
            $browser
            ->loginAs(Pessoa::comPerfil('Atendente')->first())
            ->visit('/pets')
            ->assertSee('Pets')
            ->assertSee('Pesquisar')
            ->assertSee('Novo')
            ->assertSee('ID')   
            ->assertSee('Nome')
            ->assertSee('Tipo')
            ->assertSee('Cliente')
            ->assertSee('Opções')
            ->assertSee('Total');
        });
    }

    public function testFiltraListagemDePetsPorNome()
    {
        $pet = factory(Pet::class)->create();

        $this->browse(function (Browser $browser) use ($pet) {
            $browser
            ->loginAs(Pessoa::comPerfil('Atendente')->first())
            ->visit('/pets')
            ->click('#pesquisar')
            ->waitFor('#nome')
            ->type('nome', $pet->nome)
            ->click('#filtrar')
            ->waitForText('Pets')
            ->assertSee($pet->nome)
            ;
        });
    }

    public function testBuscaPetInexistente()
    {
        $this->browse(function (Browser $browser) {
            $browser
            ->loginAs(Pessoa::comPerfil('Atendente')->first())
            ->visit('/pets')
            ->click('#pesquisar')
            ->waitFor('#nome')
            ->type('nome', 'Pet inexistente')
            ->click('#filtrar')
            ->waitFor('#novo')
            ->assertSee('Pets')
            ->assertSee('Nenhum registro encontrado');
        });
    }


    public function testCadastraPet()
    {
        $tipoPet = TipoPet::where('nome','Gato')->first();

        $this->browse(function (Browser $browser) use ($tipoPet) {
            $browser
            ->loginAs(Pessoa::comPerfil('Atendente')->first())
            ->visit('/pets')
            ->click('#novo')
            ->waitFor('#submit')
            ->type('nome', 'gato ' .date('YmdHis'))
            ->select('tipo_pet', $tipoPet->id)
            ->type('porte', '')
            ->select('cliente', Pessoa::comPerfil('Cliente')->first()->id)
            ->type('observacoes', 'Não gosta de água')
            ->check('permite_foto')
            ->check('ativo')
            ->click('#submit')
            ->waitFor('.alert-success')
            ->assertSee('Pet cadastrado com sucesso')
            ;
        });
    }

    public function testCadastraCachorroSemPorte()
    {
        $tipoPet = TipoPet::where('nome','Cachorro')->first();

        $this->browse(function (Browser $browser) use ($tipoPet) {
            $browser
            ->loginAs(Pessoa::comPerfil('Atendente')->first())
            ->visit('/pets')
            ->click('#novo')
            ->waitFor('#submit')
            ->type('nome', 'cachorro sem porte ' .date('YmdHis'))
            ->select('tipo_pet', $tipoPet->id)
            ->type('porte', '')
            ->select('cliente', Pessoa::comPerfil('Cliente')->first()->id)
            ->type('observacoes', '')
            ->check('permite_foto')
            ->check('ativo')
            ->click('#submit')
            ->waitFor('#submit')
            ->assertSee('O campo porte é obrigatório')
            ;
        });
    }

    public function testEditaPetComDadosValidos()
    {
        $pet = factory(Pet::class)->create([
            'id_tipo_pet' => TipoPet::where('nome', 'Cachorro')->first()->id
        ]);

        $this->browse(function (Browser $browser) use ($pet) {
            $browser
            ->loginAs(Pessoa::comPerfil('Atendente')->first())
            ->visit('/pets/'.$pet->id.'/edit')
            ->waitFor('#submit')
            ->type('nome', 'cachorro com porte '. date('YmdHis'))
            ->select('tipo_pet', $pet->id_tipo_pet)
            ->type('porte', 'Grande')
            ->type('observacoes', 'Gosta de filé')
            ->uncheck('permite_foto')
            ->uncheck('ativo')
            ->click('#submit')
            ->waitFor('.alert-success')
            ->assertSee('Pet atualizado com sucesso')
            ;
        });

        $pet->refresh();

        $this->browse(function (Browser $browser) use ($pet) {
            $browser
            ->loginAs(Pessoa::comPerfil('Atendente')->first())
            ->visit('/pets/'.$pet->id.'/edit')
            ->waitFor('#submit')
            ->assertInputValue('nome', $pet->nome)
            ->assertValue('#tipo_pet', $pet->id_tipo_pet)
            ->assertInputValue('porte', 'Grande')
            ->assertValue('#cliente', $pet->id_pessoa)
            ->assertInputValue('observacoes', 'Gosta de filé')
            ->assertNotChecked('permite_foto')
            ->assertNotChecked('ativo')
            ;
        });
        
    }

    public function testEditaPetSemInformarDados()
    {
        $pet = factory(Pet::class)->create([
            'id_tipo_pet' => TipoPet::where('nome', 'Cachorro')->first()->id
        ]);

        $this->browse(function (Browser $browser) use ($pet) {
            $browser
            ->loginAs(Pessoa::comPerfil('Atendente')->first())
            ->visit('/pets/'.$pet->id.'/edit')
            ->waitFor('#submit')
            ->type('nome', '')
            ->select('tipo_pet', $pet->id_tipo_pet)
            ->select('cliente')
            ->type('observacoes', '')
            ->check('permite_foto')
            ->check('ativo')
            ->click('#submit')
            ->waitFor('#submit')
            ->pause(5000)
            ->assertSee('O campo nome é obrigatório')
            ->assertSee('O campo porte é obrigatório')
            ;
        });
    }

    public function testExcluiPetComAgendamento()
    {
        $pet = factory(Pet::class)->create([
            'nome'=> 'pet ' . date('YmdHis')
        ]);

        $agendamento = factory(Agendamento::class)->create([
            'id_pet' => $pet->id
        ]);

        $this->browse(function (Browser $browser) use ($pet) {
            $browser
            ->loginAs(Pessoa::comPerfil('Atendente')->first())
            ->visit('/pets')
            ->click('#pesquisar')
            ->waitFor('#nome')
            ->type('nome', $pet->nome)
            ->click('#filtrar')
            ->waitFor('#novo')
            ->assertSee($pet->nome)
            ->click('#'.$pet->id)
            ->acceptDialog()
            ->waitFor('.alert-danger')
            ->waitForText('O pet possui agendamentos')
            ;
        });
    }

    public function testExcluiPetSemAgendamento()
    {
        $pet = factory(Pet::class)->create([
            'nome'=> 'pet ' . date('YmdHis')
        ]);

        $this->browse(function (Browser $browser) use ($pet) {
            $browser
            ->loginAs(Pessoa::comPerfil('Atendente')->first())
            ->visit('/pets')
            ->click('#pesquisar')
            ->waitFor('#nome')
            ->type('nome', $pet->nome)
            ->click('#filtrar')
            ->waitFor('#novo')
            ->assertSee($pet->nome)
            ->click('#'.$pet->id)
            ->acceptDialog()
            ->waitFor('.alert-success')
            ->waitForText('Pet excluído com sucesso')
            ;
        });
    }


}
