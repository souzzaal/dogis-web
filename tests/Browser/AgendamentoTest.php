<?php

namespace Tests\Browser;

use App\Models\AgendamentoServico;
use App\Models\AgendamentoSituacao;
use App\Models\Perfil;
use App\Models\Pessoa;
use App\Models\Pet;
use App\Models\Servico;
use App\Models\TipoPet;
use App\Models\UnidadeTempo;
use Carbon\Carbon;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AgendamentoTest extends DuskTestCase
{

    public function testListagemDeAgendamentos()
    {
        $agendamentoServicos = factory(AgendamentoServico::class)->create([
                'data_inicio' => Carbon::now()->modify('+3 hour')->format('Y-m-d H:i:s')
            ]);

        $this->browse(function(Browser $browser){
            $browser
            ->loginAs(Pessoa::comPerfil('Atendente')->first())
            ->visit('/agendamentos')
            ->assertSee('Agendamentos')
            ->assertSee('Pesquisar')
            ->assertSee('Novo')
            ->assertSee('Data')
            ->assertSee('ID')
            ->assertSee('Pet')
            ->assertSee('Tipo')
            ->assertSee('Cliente')
            ->assertSee('Serviços')
            ->assertSee('Opções')
            ;
        });

        $agendamentoServicos->delete();
    }

    public function testCadastraAgendamento()
    {   
        $ano = 3000 + intval(date('Hi'));

        $carbon = Carbon::now()->modify("+$ano year");

        $this->browse(function(Browser $browser) use ($carbon){

            $browser
            ->loginAs(Pessoa::comPerfil('Atendente')->first())
            ->visit('/agendamentos')
            ->click('#novo')
            ->waitFor('#submit')
            ->type('data', $carbon->format('d/m/Y'))
            ->value('form #horario', $carbon->format('H:i'))
            ->click('#submit')
            ->waitForText('Agendamentos')
            ->assertSee('Agendamento cadastrado com sucesso')
            ;
        });
    }

    public function testCadastraAgendamentoComDataPassada()
    {  

        $carbon = Carbon::now()->modify('-1 minute');

        $funcionario = Pessoa::comPerfil('Veterinário')->first();

        $pet = Pet::first();
        
        \DB::table('pessoas_tipos_pets')->insert([
            'id_pessoa' => $funcionario->id,
            'id_tipo_pet' => $pet->id_tipo_pet
        ]);

        $servico = factory(Servico::class)->create([
            'id_perfil' => $funcionario->id,
            'tempo_estimado' => 1,
            'id_unidade_tempo' => UnidadeTempo::where('nome', 'Horas')->first()->id
        ]);

        $this->browse(function(Browser $browser) use ($carbon, $servico, $funcionario, $pet){

            $browser
            ->loginAs(Pessoa::comPerfil('Atendente')->first())
            ->visit('/agendamentos')
            ->click('#novo')
            ->waitFor('#submit')
            ->type('data', $carbon->format('d/m/Y'))
            ->value('#pet', $pet->id)
            ->value('form #servico', $servico->id_produto_servico)
            ->value('form #funcionario', $funcionario->id)
            ->value('form #horario', $carbon->format('H:i'))
            ->click('#submit')
            ->waitForText('Agendamento')
            ->waitFor('.alert')
            ->assertSee('Não é possível agendar para datas passadas')
            ;
        });
    }

    public function testCadastraAgendamentoComFuncionarioNaoAtendeServico()
    {
        $carbon = Carbon::now()->modify('+1000 years');

        $servico = factory(Servico::class)->create([
            'id_perfil' => Perfil::where('nome', 'Veterinário')->first()->id,
            'tempo_estimado' => 1,
            'id_unidade_tempo' => UnidadeTempo::where('nome', 'Horas')->first()->id
        ]);

        $funcionario = Pessoa::comPerfil('Atendente')->first();

        $this->browse(function(Browser $browser) use ($carbon, $servico, $funcionario){

            $browser
            ->loginAs(Pessoa::comPerfil('Atendente')->first())
            ->visit('/agendamentos/create')
            ->waitFor('#submit')
            ->type('data', $carbon->format('d/m/Y'))
            ->value('form #servico', $servico->id)
            ->value('form #funcionario', $funcionario->id)
            ->value('form #horario', '08:00')
            ->click('#submit')
            ->waitForText('Agendamento')
            ->assertSee('não pode atender o serviço')
            ;
        });
    }

    public function testCadastraAgendamentoComVeterinarioNaoAtendeTipoPet()
    {
        $carbon = Carbon::now()->modify('+1000 years');

        $perfil = Perfil::where('nome', 'Veterinário')->first();

        $servico = factory(Servico::class)->create([
            'id_perfil' => $perfil->id,
            'tempo_estimado' => 1,
            'id_unidade_tempo' => UnidadeTempo::where('nome', 'Horas')->first()->id
        ]);

        $pet = factory(Pet::class)->create([
            'id_tipo_pet' => factory(TipoPet::class)->create()->id
        ]);

        $funcionario = Pessoa::comPerfil('Veterinário')->first();

        $this->browse(function(Browser $browser) use ($carbon, $servico, $pet, $funcionario){
            $browser
            ->loginAs(Pessoa::comPerfil('Atendente')->first())
            ->visit('/agendamentos/create')
            ->waitFor('#submit')
            ->type('data', $carbon->format('d/m/Y'))
            ->value('#pet', $pet->id)
            ->value('form #servico', $servico->id)
            ->value('form #funcionario', $funcionario->id)
            ->value('form #horario', $carbon->format('H:i'))
            ->click('#submit')
            ->waitForText('Agendamento')
            ->assertSee('não é habilitado para atender pets do tipo')
            ;
        });
    }

    public function testEditarAgendamentoComHorarioAnteriorConflitante()
    {
        $servico = factory(Servico::class)->create([
            'id_perfil' => Perfil::where('nome', 'Atendente')->first()->id,
            'tempo_estimado' => 1,
            'id_unidade_tempo' => UnidadeTempo::where('nome', 'Horas')->first()->id
        ]);

        $funcionario = Pessoa::comPerfil('Atendente')->first();

        $agendamentoServico1 = factory(AgendamentoServico::class)->create([
            'data_inicio' => Carbon::now()->modify('+1000 year')->format('Y-m-d H:i:s'),
            'id_servico' => $servico->id_produto_servico,
            'id_pessoa' => $funcionario->id
        ]);

        $agendamentoServico2 = factory(AgendamentoServico::class)->create([
            'data_inicio' => Carbon::now()->modify('+1000 year')->modify('+120 minute')->format('Y-m-d H:i:s'),
            'id_servico' => $servico->id_produto_servico,
            'id_pessoa' => $funcionario->id
        ]);

        $this->browse(function(Browser $browser) use ($agendamentoServico1){

            $browser
            ->loginAs(Pessoa::comPerfil('Atendente')->first())
            ->visit('/agendamentos/'.$agendamentoServico1->id_agendamento.'/edit')
            ->waitFor('#submit')
            ->value('form #horario', $agendamentoServico1->data_inicio->modify('+125 minute')->format('H:i'))
            ->click('#submit')
            ->waitForText('Agendamento')
            ->assertSee('não tem disponibilidade para atender')
            ;
        });
    }

    public function testEditarAgendamentoComHorarioPosteriorConflitante()
    {
        $servico = factory(Servico::class)->create([
            'id_perfil' => Perfil::where('nome', 'Atendente')->first()->id,
            'tempo_estimado' => 1,
            'id_unidade_tempo' => UnidadeTempo::where('nome', 'Horas')->first()->id
        ]);

        $funcionario = Pessoa::comPerfil('Atendente')->first();

        $agendamentoServico1 = factory(AgendamentoServico::class)->create([
            'data_inicio' => Carbon::now()->modify('+1000 year')->format('Y-m-d H:i:s'),
            'id_servico' => $servico->id_produto_servico,
            'id_pessoa' => $funcionario->id
        ]);

        $agendamentoServico2 = factory(AgendamentoServico::class)->create([
            'data_inicio' => Carbon::now()->modify('+1000 year')->modify('+115 minute')->format('Y-m-d H:i:s'),
            'id_servico' => $servico->id_produto_servico,
            'id_pessoa' => $funcionario->id
        ]);

        $this->browse(function(Browser $browser) use ($agendamentoServico1){

            $browser
            ->loginAs(Pessoa::comPerfil('Atendente')->first())
            ->visit('/agendamentos/'.$agendamentoServico1->id_agendamento.'/edit')
            ->waitFor('#submit')
            ->value('form #horario', $agendamentoServico1->data_inicio->modify('+125 minute')->format('H:i'))
            ->click('#submit')
            ->waitForText('Agendamento')
            ->assertSee('não tem disponibilidade para atender')
            ;
        });
    }

    public function testEditarAgendamentoSemDados()
    {
        $agendamentoServico = factory(AgendamentoServico::class)->create();

        $this->browse(function(Browser $browser) use ($agendamentoServico){
            $browser
            ->loginAs(Pessoa::comPerfil('Atendente')->first())
            ->visit('/agendamentos/'.$agendamentoServico->id_agendamento.'/edit')
            ->waitForText('Agendamento')
            ->waitFor('#submit')
            ->value('form #horario', '')
            ->click('#submit')
            ->waitForText('Agendamento')
            ->waitFor('.alert')
            ->assertSee('Para cada serviço deve ser informado o funcionário, o horário e a situação')
            ;
        });
    }

    public function testTornarAusenteAgendamentoFuturo()
    {
        $agendamentoServico = factory(AgendamentoServico::class)->create([
            'id_servico' => Servico::where('id_perfil', Perfil::where('nome', 'Atendente')->first()->id)->first()->id,
            'id_pessoa' => Pessoa::comPerfil('Atendente')->first()->id
        ]);
        $situacao = AgendamentoSituacao::where('nome', 'Ausente')->first();

        $this->browse(function(Browser $browser) use ($agendamentoServico, $situacao) {
            $browser
            ->loginAs(Pessoa::comPerfil('Atendente')->first())
            ->visit('/agendamentos/'.$agendamentoServico->id_agendamento.'/edit')
            ->waitForText('Agendamento')
            ->waitFor('#submit')
            ->value('form #situacao', $situacao->id)
            ->click('#submit')
            ->waitForText('Agendamento')
            ->assertSee('A situação "Ausente" é inválida para agendamento futuro')
            ;
        });
    }

    public function testTornarRealizadoAgendamentoFuturo()
    {
        $agendamentoServico = factory(AgendamentoServico::class)->create([
            'id_servico' => Servico::where('id_perfil', Perfil::where('nome', 'Atendente')->first()->id)->first()->id,
            'id_pessoa' => Pessoa::comPerfil('Atendente')->first()->id
        ]);
        $situacao = AgendamentoSituacao::where('nome', 'Realizado')->first();

        $this->browse(function(Browser $browser) use ($agendamentoServico, $situacao) {
            $browser
            ->loginAs(Pessoa::comPerfil('Atendente')->first())
            ->visit('/agendamentos/'.$agendamentoServico->id_agendamento.'/edit')
            ->waitForText('Agendamento')
            ->waitFor('#submit')
            ->value('form #situacao', $situacao->id)
            ->click('#submit')
            ->waitForText('Agendamento')
            ->assertSee('A situação "Realizado" é inválida para agendamento futuro')
            ;
        });
    }

    public function testExcluirAgendamentoComServicosFuturos()
    {
        $agendamentoServico = factory(AgendamentoServico::class)->create();

        $this->browse(function (Browser $browser) use ($agendamentoServico) {
            $browser
            ->loginAs(Pessoa::comPerfil('Atendente')->first())
            ->visit('/agendamentos')
            ->waitFor('#data')
            ->type('data', $agendamentoServico->data_inicio->format('d/m/Y'))
            ->click('#filtrar')
            ->waitFor('#novo')
            ->assertSee($agendamentoServico->data_inicio->format('H:i'))
            ->click('#'.$agendamentoServico->id_agendamento)
            ->acceptDialog()
            ->waitFor('#novo')
            ->waitFor('.alert-success')
            ->assertSee('Agendamento excluído com sucesso')
            ;
        });
    }

    public function testExcluirAgendamentoComServicosPassados()
    {
        $agendamentoServico = factory(AgendamentoServico::class)->create([
            'data_inicio' => Carbon::now()->modify('-1 minute')->format('Y-m-d H:i:s')
        ]);

        $this->browse(function (Browser $browser) use ($agendamentoServico) {
            $browser
            ->loginAs(Pessoa::comPerfil('Atendente')->first())
            ->visit('/agendamentos')
            ->waitFor('#data')
            ->type('data', $agendamentoServico->data_inicio->format('d/m/Y'))
            ->click('#filtrar')
            ->waitFor('#novo')
            ->pause(1000)
            ->assertSee($agendamentoServico->data_inicio->format('H:i'))
            ->click('#'.$agendamentoServico->id_agendamento)
            ->acceptDialog()
            ->waitFor('#novo')
            ->assertSee('O agendamento possui um ou mais serviços com a data passada')
            ;
        });

        $agendamentoServico->delete();
    }



    // public function testBuscaAgendamentoEmDataInvalida()
    // {
    //     $this->browse(function(Browser $browser){
    //         $browser
    //         ->loginAs(Pessoa::comPerfil('Atendente')->first())
    //         ->visit('/agendamentos')
    //         ->type('data', '31/02/2019')
    //         ->click('#filtrar')
    //         ->pause(1000)
    //         ->assertSee('Data inválida')
    //         ;
    //     });
    // }




}
