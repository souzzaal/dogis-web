<?php

namespace Tests\Browser;

use App\Models\AgendamentoServico;
use App\Models\Perfil;
use App\Models\Pessoa;
use App\Models\Servico;
use App\Models\UnidadeTempo;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ServicoTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testListagemDeServicosParaAdministrador()
    {
        $this->browse(function (Browser $browser) {
            $browser
            ->loginAs(Pessoa::comPerfil('Administrador')->first())
            ->visit('/servicos')
            ->assertSee('Serviços')
            ->assertSee('Pesquisar')
            ->assertSee('Novo')
            ->assertSee('ID')
            ->assertSee('Nome')
            ->assertSee('Tempo estimado')
            ->assertSee('Preço')
            ->assertSee('Opções')
            ->assertSee('Total');
        });
    }
     
    public function testFiltraListagemDeServicosPorNome()
    {
        $servico = factory(Servico::class)->create();

        $this->browse(function (Browser $browser) use ($servico) {
            $browser
            ->loginAs(Pessoa::comPerfil('Administrador')->first())
            ->visit('/servicos')
            ->click('#pesquisar')
            ->waitFor('#nome')
            ->type('nome', $servico->produtoServico->nome)
            ->click('#filtrar')
            ->pause(2000)
            ->assertSee('Serviços')
            ->assertSee($servico->produtoServico->nome)
            ->assertSee('Total: 1 registro encontrado.');
        });
    }
     
    public function testBuscaServicoInexistente()
    {
        $this->browse(function (Browser $browser) {
            $browser
            ->loginAs(Pessoa::comPerfil('Administrador')->first())
            ->visit('/servicos')
            ->click('#pesquisar')
            ->waitFor('#nome')
            ->type('nome', 'Serviço inexistente')
            ->click('#filtrar')
            ->waitFor('#novo')
            ->assertSee('Serviços')
            ->assertSee('Nenhum registro encontrado');
        });
    }

    public function testCadastraServicoComDadosValidos()
    {
        $this->browse(function (Browser $browser) {
            $browser
            ->loginAs(Pessoa::comPerfil('Administrador')->first())
            ->visit('/servicos')
            ->click('#novo')
            ->waitFor('#submit')
            ->type('nome', 'servico ' . date('YmdHis'))
            ->type('preco', '10,00')
            ->type('tempo_estimado', '10')
            ->type('pontos_ganhos', '10')
            ->type('pontos_necessarios', '50')
            ->click('#submit')
            ->waitForText('Serviço cadastrado com sucesso')
            ->assertSee('Serviço cadastrado com sucesso');
        });
    }

    public function testCadastraServicoSemDados()
    {
        $this->browse(function (Browser $browser) {
            $browser
            ->loginAs(Pessoa::comPerfil('Administrador')->first())
            ->visit('/servicos')
            ->click('#novo')
            ->waitFor('#submit')
            ->click('#submit')
            ->waitFor('#submit')
            ->assertSee('O campo nome é obrigatório.')
            ->assertSee('O campo preco é obrigatório.')
            ->assertSee('O campo tempo estimado é obrigatório.')
            ->assertSee('O campo pontos ganhos é obrigatório.')
            ->assertSee('O campo pontos necessarios é obrigatório.')
            ;
        });
    }

    public function testEditaServicoComDadosValidos()
    {
        $servico = factory(Servico::class)->create();
        $unidade_tempo = UnidadeTempo::where('nome', 'Minutos')->first()->id;
        $perfil = Perfil::where('nome', 'Veterinário')->first()->id;

        $this->browse(function (Browser $browser) use ($servico, $unidade_tempo, $perfil) {
            $browser
            ->loginAs(Pessoa::comPerfil('Administrador')->first())
            ->visit("/servicos/{$servico->id_produto_servico}/edit")
            ->waitFor('#submit')
            ->type('nome', 'servico ' . date('YmdHis'))
            ->type('preco', '20,00')
            ->type('tempo_estimado', '20')
            ->select('unidade_tempo', $unidade_tempo)
            ->type('pontos_ganhos', '20')
            ->type('pontos_necessarios', '100')
            ->select('atendido_por', $perfil)
            ->uncheck('ativo')
            ->click('#submit')
            ->waitFor('#novo')
            ->waitForText('Serviço atualizado com sucesso');
        });

        $servico->refresh();

        $this->browse(function (Browser $browser) use ($servico, $unidade_tempo, $perfil) {
            $browser
            ->loginAs(Pessoa::comPerfil('Administrador')->first())
            ->visit("/servicos/{$servico->id_produto_servico}/edit")
            ->waitFor('#submit')
            ->assertInputValue('nome', $servico->produtoServico->nome)
            ->assertInputValue('preco', $servico->preco)
            ->assertInputValue('tempo_estimado', $servico->tempo_estimado)
            ->assertValue('#unidade_tempo', $unidade_tempo)
            ->assertInputValue('pontos_ganhos', 20)
            ->assertInputValue('pontos_necessarios', $servico->pontos_necessarios)
            ->assertValue('#atendido_por', $perfil)
            ->assertNotChecked('ativo')
            ; 
        });
    }

    public function testExcluiServicoSemAgendamento()
    {
        $servico = factory(Servico::class)->create();

        $this->browse(function (Browser $browser) use ($servico) {
            $browser
            ->loginAs(Pessoa::comPerfil('Administrador')->first())
            ->visit('/servicos')
            ->click('#pesquisar')
            ->waitFor('#nome')
            ->type('nome', $servico->produtoServico->nome)
            ->click('#filtrar')
            ->waitFor('#novo')
            ->assertSee($servico->produtoServico->nome)
            ->click('#'.$servico->id_produto_servico)
            ->acceptDialog()
            ->waitFor('#novo')
            ->waitForText('Serviço excluído com sucesso')
            ;
        });
    }

    public function testExcluiServicoComAgendamento()
    {
        $servico = factory(Servico::class)->create();
        $agendamentoServico = factory(AgendamentoServico::class)->create([
            'id_servico' => $servico->id_produto_servico
        ]);

        $this->browse(function (Browser $browser) use ($servico) {
            $browser
            ->loginAs(Pessoa::comPerfil('Administrador')->first())
            ->visit('/servicos')
            ->click('#pesquisar')
            ->waitFor('#nome')
            ->type('nome', $servico->produtoServico->nome)
            ->click('#filtrar')
            ->waitFor('#novo')
            ->assertSee($servico->produtoServico->nome)
            ->click('#'.$servico->id_produto_servico)
            ->acceptDialog()
            ->waitFor('#novo')
            ->waitForText('O serviço possui agendamentos.')
            ;
        });
    }
}
